# Setup Authentication Server

## BackEnd

1. Start sso server → save ip and port;
2. import [com.tecnositaf.sso.resource.jar](https://gitlab.tecnositaf.it/rayonit/sso/raw/master/back-end/resource-server/com.tecnositaf.sso.resource.jar) to resource server 
3. Set the properties
  - sso.server.path=installed sso server ip
  - sso.server.appname=your application name (same as application registered  at sso server)
4. create a class that extends WebSecurityConfig from om.tecnositaf.sso.resource.jar and anotate class with:

```java
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
```

5. add to your main class : `@ComponentScan(basePackages = {"com.tecnositaf.sso.resource"})`
6. insert application in web 

PS:import web and springsecurity

### FrontEnd

1. Start SSO client
2. Copy LoginComponent and Authentication Service from Resource client sample.
3. Configure Web socket connection for logout from all open tabs of your application in appComponent.ts. You can copy this code from resource server sample.
4. Set environment variables :
  - authUrl = SSO server endpoint
  - apiUrl = Resource server endpoint
