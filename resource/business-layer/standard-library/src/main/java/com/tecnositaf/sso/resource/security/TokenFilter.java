package com.tecnositaf.sso.resource.security;

import com.tecnositaf.auth.data.model.AuthResponse;
import com.tecnositaf.sso.resource.services.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class TokenFilter extends GenericFilterBean {


    @Autowired
    AuthenticationService tokenService;

    private static final Logger logger = LoggerFactory.getLogger(TokenFilter.class);


    private String resolveToken(HttpServletRequest servletRequest) {
        logger.debug("TokenProvider resolve token");
        String bearerToken = servletRequest.getHeader("authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }

   /* public Authentication getAuthentication(String authresponse) throws IOException {
        if (authresponse == null)
            return null;
        ObjectMapper mapper = new ObjectMapper();
        AuthResponse authResponse = mapper.readValue(authresponse, AuthResponse.class);
        if (authResponse.getValidated() && authResponse.getUser() != null) {
            return new UsernamePasswordAuthenticationToken(authResponse.getUser(), "", authResponse.getUser().getRoles().stream().map(s -> new SimpleGrantedAuthority("ROLE_" + s)).collect(toList()));
        } else return null;
    }*/

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //String token = resolveToken((HttpServletRequest) servletRequest);
        String token = tokenService.resolveToken((HttpServletRequest) servletRequest);
        HttpServletRequest httpServletRequest = ((HttpServletRequest) servletRequest);
        if (token == null) {
            logger.info("token null");
            SecurityContextHolder.getContext().setAuthentication(null);
        } else {

            ResponseEntity<AuthResponse> authRespEntity =  tokenService.authenticate(token, tokenService.getApplicationName(), tokenService.getAuthServiceUrl());
            if(authRespEntity.getStatusCode().is2xxSuccessful() && authRespEntity.getBody() != null){
                servletRequest.setAttribute("user",authRespEntity.getBody());
                SecurityContextHolder.getContext().setAuthentication(tokenService.getAuthentication(authRespEntity.getBody()));
            }else{
                logger.warn("Token Authentication failed");
            }
           /* RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("authorization", httpServletRequest.getHeader("authorization"));
            headers.set("url", httpServletRequest.getRequestURL().toString());
            headers.set("appName", currentApplicationName);
            HttpEntity<String> entity = new HttpEntity<>(headers);
            ResponseEntity<String> a = null;
            try {
                a = restTemplate.exchange(authService+"/api/v1/auth", HttpMethod.GET, entity, String.class);
            } catch (Exception e) {
                logger.error("Error in auth call request for url {}", authService+"/api/v1/auth", e);
                a = null;
            }

            if (a != null && a.getBody() != null)
                servletRequest.setAttribute("user", a.getBody());
            SecurityContextHolder.getContext().setAuthentication(getAuthentication(a == null ? null : a.getBody()));*/

        }
        filterChain.doFilter(servletRequest, servletResponse);

    }

}
