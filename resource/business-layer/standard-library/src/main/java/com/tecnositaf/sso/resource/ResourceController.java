package com.tecnositaf.sso.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tecnositaf.auth.data.model.AuthResponse;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
public class ResourceController {
    private static final String jwtTokenCookieName = "JWT-TOKEN";

    @CrossOrigin
    @RequestMapping(value = "/api/v1/printUserForTest", method = RequestMethod.GET)
    public AuthResponse protectedResource(HttpServletRequest servletRequest) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(servletRequest.getAttribute("user").toString(), AuthResponse.class);
    }
}
