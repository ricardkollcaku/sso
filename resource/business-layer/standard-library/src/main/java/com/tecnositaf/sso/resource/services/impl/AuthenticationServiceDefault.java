package com.tecnositaf.sso.resource.services.impl;


import com.tecnositaf.sso.resource.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Qualifier("tokenService")
public class AuthenticationServiceDefault implements AuthenticationService {

    public final String AUTH_PATH =  "/api/v1/auth";
    public final String TOKEN_PATH =  "/api/v1/users/login";


    @Value("${sso.server.path}")
    String authService;
    @Value("${sso.server.appname}")
    String applicationName;

    @Override
    public String getApplicationName() {
        return applicationName;
    }

    @Override
    public String getAuthServiceUrl() {
        return authService+AUTH_PATH;
    }

    @Override
    public String getTokenServiceUrl() {
        return authService+TOKEN_PATH;
    }
}
