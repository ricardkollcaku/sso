package com.tecnositaf.sso.resource.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tecnositaf.auth.data.dto.UserDto;
import com.tecnositaf.auth.data.dto.UsersDtoResponseObject;
import com.tecnositaf.auth.data.model.AuthResponse;
import com.tecnositaf.auth.data.model.Page;
import com.tecnositaf.auth.data.model.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserService {
    @Value("${sso.server.path}")
    String authService;
    @Value("${sso.server.appname}")
    String currentApplicationName;
    @Autowired
    ObjectMapper objectMapper;

    public UsersDtoResponseObject getAllUsers(HttpServletRequest httpServletRequest) throws IOException {
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<String> entity = new HttpEntity(getDefaultHeader(httpServletRequest));
        ResponseEntity a = null;

        try {
            String url = this.authService + "/api/v1/users/applicationUsers";
            a = restTemplate.exchange(url, HttpMethod.GET, entity, String.class, new Object[0]);
        } catch (Exception var11) {
            System.out.println("exc");
            a = null;
        }
        return a != null ? objectMapper.readValue(a.getBody().toString(), UsersDtoResponseObject.class) : null;
    }

    public UserDto getCurrentUser(HttpServletRequest httpServletRequest) throws IOException {
        return httpServletRequest.getAttribute("user") != null ? objectMapper.readValue(httpServletRequest.getAttribute("user").toString(), AuthResponse.class).getUser() : null;
    }

    private HttpHeaders getDefaultHeader(HttpServletRequest httpServletRequest) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("authorization", httpServletRequest.getHeader("authorization"));
        headers.set("url", httpServletRequest.getRequestURL().toString());
        headers.set("appName", this.currentApplicationName);
        return headers;
    }

    public Boolean isUserPresent(String username, HttpServletRequest httpServletRequest) throws IOException {


        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = getDefaultHeader(httpServletRequest);
        httpHeaders.set("username", username);
        HttpEntity<String> entity = new HttpEntity(httpHeaders);
        ResponseEntity a = null;

        try {
            String url = this.authService + "/api/v1/users/isUserPresent";
            a = restTemplate.exchange(url, HttpMethod.GET, entity, Boolean.class, new Object[0]);
        } catch (Exception var11) {
            System.out.println("exc");
            a = null;
        }
        return a != null ? (Boolean) a.getBody() : null;
    }


    public String checkFilter(String filter) throws UnsupportedEncodingException {
        if (filter != null && filter.length() > 0 && filter.charAt(0) == '{')
            filter = filter.replace("{", "").replace("}", "");
        return  URLEncoder.encode(filter, "UTF-8");
    }

    private Page<UserDto> getUserPageableFiltered(HttpServletRequest httpServletRequest, Pageable pageable, String filter) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> entity = new HttpEntity(getDefaultHeader(httpServletRequest));
        ResponseEntity a = null;
        Map<String, Object> vars = new HashMap<>();
        vars.put("pageNumber", pageable.getPageNumber());
        vars.put("elementPerPage", pageable.getElementPerPage());
        vars.put("needTotal", pageable.isNeedTotal());
        vars.put("filters", filter);
        try {
            String url = this.authService + "/api/v1/users/applicationUsers/filtered";
            a = restTemplate.exchange(url, HttpMethod.GET, entity, String.class, vars);
        } catch (Exception var11) {
            System.out.println("exc");
            a = null;
        }

        return a != null ? objectMapper.readValue(a.getBody().toString(), Page.class) : null;
    }

    public Page<UserDto> getAllUsersPagable(HttpServletRequest httpServletRequest, Pageable pageable, String filter) throws IOException {
        return getUserPageableFiltered(httpServletRequest, pageable, checkFilter(filter));
    }


}
