package com.tecnositaf.sso.resource.services;


import com.tecnositaf.auth.data.dto.LoginDto;
import com.tecnositaf.auth.data.dto.TokenDto;
import com.tecnositaf.auth.data.model.AuthResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static java.util.stream.Collectors.toList;

public interface AuthenticationService {
     static final Logger logger = LoggerFactory.getLogger(AuthenticationService.class);

    default String resolveToken(HttpServletRequest servletRequest){
         logger.debug("TokenProvider resolve token");
         String bearerToken = servletRequest.getHeader("authorization");
         if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
             return bearerToken.substring(7, bearerToken.length());
         }
         return null;
     };


    default ResponseEntity<AuthResponse> authenticate(String token, String appName, String authUrl) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("authorization","Bearer "+token);
        headers.set("appName", appName);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<AuthResponse> response = null;
        try {
            response = restTemplate.exchange(authUrl, HttpMethod.GET, entity, AuthResponse.class);
        } catch (Exception e) {
            logger.error("Error in auth call request {}", authUrl, e);
            response = null;
        }
        return response;
    }

    default ResponseEntity<TokenDto> getToken(LoginDto loginDto, String tokenUrl) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> entity = new HttpEntity(loginDto, null);
        return restTemplate.exchange(getTokenServiceUrl(), HttpMethod.POST, entity, TokenDto.class);
    }

    default  Authentication getAuthentication(AuthResponse authResponse) throws IOException {
        if (authResponse.getValidated() && authResponse.getUser() != null) {
            return new UsernamePasswordAuthenticationToken(authResponse.getUser(),
                    "", authResponse.getUser().getRoles().stream()
                    .map(s -> new SimpleGrantedAuthority("ROLE_" + s)).collect(toList()));
        } else return null;
    }

    String getApplicationName();

    String getAuthServiceUrl();

    String getTokenServiceUrl();
}
