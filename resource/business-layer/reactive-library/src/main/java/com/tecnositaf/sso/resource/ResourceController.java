package com.tecnositaf.sso.resource;


import com.tecnositaf.auth.data.dto.UserDto;
import com.tecnositaf.auth.data.model.Page;
import com.tecnositaf.auth.data.model.Pageable;
import com.tecnositaf.sso.resource.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.IOException;

@RestController
public class ResourceController {
    private static final String jwtTokenCookieName = "JWT-TOKEN";
    @Autowired
    UserService userService;

    @CrossOrigin
    @RequestMapping(value = "/api/v1/printUserForTest", method = RequestMethod.POST)
    public Mono<Page<UserDto>> protectedResource(ServerWebExchange servletRequest, @RequestBody Pageable pageable) throws IOException {

        return userService.getAllUsersPagable(servletRequest,pageable,null);
    }
}
