package com.tecnositaf.sso.resource.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tecnositaf.auth.data.model.AuthResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.function.Function;

import static java.util.stream.Collectors.toList;

@Component
public class TokenFilter implements Function<ServerWebExchange, Mono<Authentication>> {


    @Value("${sso.server.path}")
    String authService;
    @Value("${sso.server.appname}")
    String currentApplicationName;

    private Mono<String> resolveToken(ServerWebExchange servletRequest) {
    //    System.out.println("TokenProvider resolve token");
        String bearerToken = servletRequest.getRequest().getHeaders().getFirst("authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return Mono.just(bearerToken.substring(7, bearerToken.length()));
        } else {
            return Mono.empty();
        }
    }

    public Mono<Authentication> getAuthentication(String authresponse) {
        try {
            if (authresponse == null)
                return Mono.empty();
            ObjectMapper mapper = new ObjectMapper();
            AuthResponse authResponse = mapper.readValue(authresponse, AuthResponse.class);
            if (authResponse.getValidated() && authResponse.getUser() != null) {
                return Mono.just(new UsernamePasswordAuthenticationToken(authResponse.getUser(), "", authResponse.getUser().getRoles().stream().map(s -> new SimpleGrantedAuthority("ROLE_" + s)).collect(toList())));
            } else return Mono.empty();
        } catch (Exception e) {
            return Mono.empty();
        }
    }

    /* @Override
     public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
         System.out.println("auth server" + authService);
         String token = resolveToken((ServerHttpRequest) servletRequest);
         ServerHttpRequest httpServletRequest = ((ServerHttpRequest) servletRequest);
         if (token == null) {
             System.out.println("token null");
             SecurityContextHolder.getContext().setAuthentication(null);
         } else {
             RestTemplate restTemplate = new RestTemplate();
             HttpHeaders headers = new HttpHeaders();
             headers.set("authorization", httpServletRequest.getHeaders().getFirst("authorization"));
             headers.set("url", httpServletRequest.getURI().toString());
             headers.set("appName", currentApplicationName);
             HttpEntity<String> entity = new HttpEntity<>(headers);
             ResponseEntity<String> a = null;
             try {
                 a = restTemplate.exchange(authService + "/api/v1/auth", HttpMethod.GET, entity, String.class);
             } catch (Exception e) {
                 System.out.println("exc");
                 a = null;
             }
             if (a != null && a.getBody() != null)
                 servletRequest.setAttribute("user", a.getBody());
             SecurityContextHolder.getContext().setAuthentication(getAuthentication(a == null ? null : a.getBody()));

         }
         filterChain.doFilter(servletRequest, servletResponse);

     }
 */
    @Override
    public Mono<Authentication> apply(ServerWebExchange serverWebExchange) {
        return Mono.justOrEmpty(serverWebExchange)
                .flatMap(serverWebExchange1 -> resolveToken(serverWebExchange1)
                        .flatMap(s -> {
                            return WebClient.create(authService)
                                    .get()
                                    .uri("/api/v1/auth")
                                    .header("authorization", serverWebExchange1.getRequest().getHeaders().getFirst("authorization"))
                                    .header("url", serverWebExchange1.getRequest().getURI().toString())
                                    .header("appName", currentApplicationName)
                                    .exchange()
                                    .flatMap(clientResponse -> clientResponse.bodyToMono(String.class).map(s1 -> {
                               //         System.out.println("response: " + s1);
                                        return s1;
                                    }))
                                    .map(s1 -> {
                                        serverWebExchange1.getAttributes().put("user", s1);
                                        //serverWebExchange1.getRequest().getHeaders().set("user", s1);
                                        return s1;
                                    })
                                    .flatMap(s3 -> getAuthentication(s3));


                        })
                );


    }
}
