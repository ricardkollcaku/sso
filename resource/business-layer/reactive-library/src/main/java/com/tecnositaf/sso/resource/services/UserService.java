package com.tecnositaf.sso.resource.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tecnositaf.auth.data.dto.UserDto;
import com.tecnositaf.auth.data.dto.UsersDtoResponseObject;
import com.tecnositaf.auth.data.model.AuthResponse;
import com.tecnositaf.auth.data.model.Page;
import com.tecnositaf.auth.data.model.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;

@Service
public class UserService {
    @Value("${sso.server.path}")
    String authService;
    @Value("${sso.server.appname}")
    String currentApplicationName;
    @Autowired
    ObjectMapper objectMapper;

    public Flux<UserDto> getAllUsers(ServerWebExchange httpServletRequest) throws IOException {

        return WebClient.create(authService)
                .get()
                .uri("/api/v1/users/applicationUsers")
                .header("authorization", httpServletRequest.getRequest().getHeaders().getFirst("authorization"))
                .header("url", httpServletRequest.getRequest().getURI().toString())
                .header("appName", currentApplicationName)
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(UsersDtoResponseObject.class))
                .flatMapIterable(usersDtoResponseObject -> usersDtoResponseObject.getBody())
                ;
    }

    public UserDto getCurrentUser(ServerWebExchange httpServletRequest) throws IOException {
        return httpServletRequest.getAttributes().get("user") != null ? objectMapper.readValue(httpServletRequest.getAttributes().get("user").toString(), AuthResponse.class).getUser() : null;
    }

    public Mono<Boolean> isUserPresent(String username, ServerWebExchange httpServletRequest) throws IOException {

        return WebClient.create(authService)
                .get()
                .uri("/api/v1/users/isUserPresent")
                .header("authorization", httpServletRequest.getRequest().getHeaders().getFirst("authorization"))
                .header("url", httpServletRequest.getRequest().getURI().toString())
                .header("appName", currentApplicationName)
                .header("username", username)
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(Boolean.class));
    }

    public String checkFilter(String filter) {
        if (filter != null && filter.length() > 0 && filter.charAt(0) == '{')
            filter = filter.replace("{", "").replace("}", "");
        return filter;
    }

    private Mono<Page<UserDto>> getUserPageableFiltered(ServerWebExchange httpServletRequest, Pageable pageable, String filter) {

        return WebClient.create(authService)
                .get()
                .uri(uriBuilder -> uriBuilder.path("/api/v1/users/applicationUsers/filtered")
                        .queryParam("pageNumber", pageable.getPageNumber())
                        .queryParam("elementPerPage", pageable.getElementPerPage())
                        .queryParam("needTotal", pageable.isNeedTotal())
                        .queryParam("filters", filter)
                        .build()
                )
                .header("authorization", httpServletRequest.getRequest().getHeaders().getFirst("authorization"))
                .header("url", httpServletRequest.getRequest().getURI().toString())
                .header("appName", currentApplicationName)
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(Page.class))
                .map(page -> {
                    Page<UserDto> page1 = page;
                    return page1;
                });
    }

    public Mono<Page<UserDto>> getAllUsersPagable(ServerWebExchange httpServletRequest, Pageable pageable, String filter) throws IOException {
        return getUserPageableFiltered(httpServletRequest, pageable, checkFilter(filter));
    }
}
