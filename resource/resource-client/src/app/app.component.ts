﻿import {Component} from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {Router} from '@angular/router';
import {environment} from '../environments/environment';

@Component({
  selector: 'app',
  templateUrl: 'app.component.html'
})

export class AppComponent {
  private serverUrl: String;
  private token;
  private stompClient;

  constructor(private router: Router) {

    if (localStorage.getItem('currentUser')) {
      this.token = JSON.parse(localStorage.getItem('currentUser'));
    }
    this.serverUrl = environment.socketEndpoint;
    this.initializeWebSocketConnection();

  }

  initializeWebSocketConnection() {
    const ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/logout',
        token => {
          const a = JSON.parse(localStorage.getItem('currentUser'));
          if (a === null) {
            that.router.navigate(['login']);
          } else if (token.body === a.body) {
            localStorage.removeItem('currentUser');
            that.router.navigate(['login']);
          }
        });
    });
  }

}
