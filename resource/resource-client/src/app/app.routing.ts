﻿import {Routes, RouterModule} from '@angular/router';

import {HomeComponent} from './components/home';
import {AuthGuard} from './_guards';
import {LoginComponent} from './components/login/login.component';


const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: '**', redirectTo: ''}
];

export const routing = RouterModule.forRoot(appRoutes);
