import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  returnUrl: String;

  constructor() {
  }

  ngOnInit() {
    this.returnUrl = 'http://localhost:4201/home';
    window.location.href = 'http://localhost:4200/login/?url=' + this.returnUrl;
  }
}


