﻿import {Component, OnInit} from '@angular/core';
import {User} from '../../_models/index';
import {AuthenticationService, RoleService} from '../../_services/index';
import {ActivatedRoute, Router} from '@angular/router';

@Component({templateUrl: 'home.component.html'})
export class HomeComponent implements OnInit {
  currentUser: User;
  token: any;
  userData: any;

  constructor(private route: ActivatedRoute,
              private authenticationService: AuthenticationService,
              private router: Router,
              private roleService: RoleService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    if (!localStorage.getItem('currentUser')) {
      this.token = this.route.snapshot.queryParams['token'];
      if (this.token === null || this.token === undefined) {
        this.router.navigate(['/login']);
      } else {
        localStorage.setItem('currentUser', this.token);
      }
    }
    this.getTest();
  }

  logout() {
    this.authenticationService.logout().subscribe(data => {
        localStorage.removeItem('currentUser');
        this.router.navigate(['login']);
      },
      error => {
      },
    );
  }

  getTest() {
    this.roleService.getUserData()
      .subscribe(apps => {
        this.userData = apps;
      }, error1 => {
        console.log('errori esht' + error1);
      });
  }
}
