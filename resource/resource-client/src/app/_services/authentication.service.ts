﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient) {
  }

  logout() {
    return this.http.get(`${environment.authUrl}/logout`);
  }
}
