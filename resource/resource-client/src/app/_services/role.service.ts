import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class RoleService {
  constructor(private http: HttpClient) {
  }

  getUserData() {
    return this.http.get(`${environment.api}/printUserForTest`);
  }
}
