package it.tecnositaf.sso.frontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsoFrontendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsoFrontendApplication.class, args);
    }
}
