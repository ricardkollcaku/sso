import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthenticationService} from '../_services';
import {ActivatedRoute} from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  returnUrl: any;

  constructor(private authService: AuthenticationService, private route: ActivatedRoute) {
    this.returnUrl = this.route.snapshot.queryParams['url'] || '';

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUser.token}`
        }
      });
    }
    return next.handle(request)
      .pipe(
        catchError(
          (error: any, caught: Observable<HttpEvent<any>>) => {
            if (error.status === 403 || error.status === 0) {
              this.authService.refreshToken(this.returnUrl).subscribe();
              return of(error);
            }
            throw error;
          }
        ),
      );
  }


}

