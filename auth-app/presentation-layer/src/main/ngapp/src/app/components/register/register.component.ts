﻿import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AlertService, UserService} from '../../_services';
import {User} from '../../_models';
import {environment} from '../../../environments/environment';

@Component({templateUrl: 'register.component.html'})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  isFirstUser;
  newUserData: User;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService) {
    this.check();

  }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      userName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.newUserData = this.registerForm.value;
    if (this.isFirstUser === true) {
      this.newUserData.systemRole = environment.systemRole;
    }
    this.userService.adminRegister(this.newUserData)
      .pipe(first())
      .subscribe(
        data => {
          if (data.status === 200) {
            this.alertService.success('Registration successful', true);
            this.router.navigate(['/login']);
          } else {
            this.alertService.error('User exists');
            this.loading = false;
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  check() {
    this.userService.checkAdmin()
      .subscribe(
        data => {
          this.isFirstUser = data;
          this.redirect();
        }
      );
  }

  redirect() {
    if (this.isFirstUser === true) {
      this.alertService.error('You are not allowed', true);
      this.router.navigate(['/login']);
    }
  }
}
