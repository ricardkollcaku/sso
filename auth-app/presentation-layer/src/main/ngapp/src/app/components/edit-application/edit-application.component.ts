import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AlertService} from "../../_services";
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from "@angular/material";
import {Application} from "../../_models/Application";
import {ApplicationServiceService} from "../../_services/application-service.service";

@Component({
  selector: 'app-edit-application',
  templateUrl: './edit-application.component.html',
  styleUrls: ['./edit-application.component.css']
})
export class EditApplicationComponent implements OnInit {

  public editApplicationForm: FormGroup;
  myApplication: Application;

  constructor(private formBuilder: FormBuilder,
              private alertService: AlertService,
              private applicationService: ApplicationServiceService,
              public dialogRef: MatDialogRef<Application>,
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) private data: Application) {
    this.myApplication = data;
  }

  ngOnInit() {
    this.initForm()
  }

  g() {
    return this.editApplicationForm.controls
  }

  onSubmit() {

    // stop here if form is invalid
    if (this.editApplicationForm.invalid) {
      return;
    }
    this.myApplication.applicationName = this.editApplicationForm.value.applicationName;
    this.myApplication.applicationUrl = this.editApplicationForm.value.applicationUrl;
    this.myApplication.logoutUrl = this.editApplicationForm.value.logoutUrl;
    this.applicationService.editApplication(this.myApplication)
      .subscribe(value => {
          console.log(value);
          // this.alertService.success('Update successful', true);
        this.snackBar.open("Update successful", null, {
          duration: 2000
        });
          this.dialogRef.close();

        },
        error1 => {
          this.alertService.error(error1, true);
        })

  }

  private initForm() {
    this.editApplicationForm = this.formBuilder.group({
      applicationUrl: [this.data.applicationUrl, Validators.required],
      applicationName: [this.data.applicationName, Validators.required],
      logoutUrl: [this.data.logoutUrl, Validators.required]
    });
  }

}
