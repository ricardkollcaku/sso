import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../_services';
import {Router} from '@angular/router';
import {MatDialog} from "@angular/material";
import {ChangePassComponent} from "../change-pass/change-pass.component";
import {User} from "../../_models/user";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  homeUrl: string;
  appUrl: string;
  roleUrl: string;
  admin = 'home';

  constructor(private authService: AuthenticationService,
              private router: Router,
              public dialog: MatDialog) {
    try {
      this.admin = JSON.parse(localStorage.getItem('userData')).systemRole;
      if (this.admin === 'admin') {
        this.homeUrl = 'admin/home';
      } else if (this.admin === 'applicationAdmin') {
        this.homeUrl = 'appAdmin/home';
      } else {
        this.homeUrl = 'home';
      }
    } catch (e) {
      this.homeUrl = 'home';
    }

  }

  getRole() {
    try {
      return this.admin = JSON.parse(localStorage.getItem('userData')).systemRole;
    } catch (e) {
      return 'user';
    }
  }


  ngOnInit() {
  }


  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  isAdmin() {
    return this.getRole() === 'admin';
  }

  getUserFullName(): string {
    const user: User =  JSON.parse(localStorage.getItem('userData'));
    return user.name + " "+ user.lastName;
  }

  logout() {
    this.authService.logout().subscribe(data => {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('userData');
        this.router.navigate(['login']);
      },
      error => {
      },
    );
  }

  changePass() {
    this.dialog.open(ChangePassComponent, {
      width: '550px'
    });
  }
}
