import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from "@angular/forms";
import {AlertService, UserService} from "../../_services";
import {ErrorStateMatcher, MatDialogRef} from "@angular/material";
import {ChangePassDto} from "../../_models/changePassDto";

@Component({
  selector: 'app-change-pass',
  templateUrl: './change-pass.component.html',
  styleUrls: ['./change-pass.component.css']
})
export class ChangePassComponent implements OnInit {
  public changePassForm: FormGroup;
  changePassDto: ChangePassDto;

  constructor(private formBuilder: FormBuilder,
              private alertService: AlertService,
              private userService: UserService,
              public dialogRef: MatDialogRef<ChangePassComponent>) {

  }

  ngOnInit() {
    this.initForm()
  }

  g(){
    return this.changePassForm.controls
  }
  private initForm() {
    this.changePassForm = this.formBuilder.group({
      oldPass: ['', Validators.required],
      newPass: ['', Validators.required],
      confirmPass: ['']
    }, {
      validator: this.checkPasswords
    });
  }

  private checkPasswords(group: FormGroup) {
    let pass = group.controls.newPass.value;
    let confirmPass = group.controls.confirmPass.value;

    if(pass !== confirmPass) {
      group.controls.confirmPass.setErrors({
        notMatched: true
      })
    }
  }

  onSubmit() {

    // stop here if form is invalid
    if (this.changePassForm.invalid) {
      return;
    }

    this.changePassDto = this.changePassForm.value;
    this.userService.changePass(this.changePassDto)
      .subscribe(value => {
        console.log(value);
            this.alertService.success('Update successful', true);
          this.dialogRef.close();

        },
        error1 => {
          this.alertService.error(error1, true);
        })

  }
}
