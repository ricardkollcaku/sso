import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AlertService, UserService} from '../../_services';
import {first} from 'rxjs/operators';
import {ApplicationServiceService} from '../../_services/application-service.service';

@Component({
  selector: 'app-add-admin',
  templateUrl: './add-admin.component.html',
  styleUrls: ['./add-admin.component.css']
})
export class AddAdminComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  apps;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService,
    private applicationService: ApplicationServiceService
  ) {
  }

  get f() {
    return this.registerForm.controls;
  }

  ngOnInit() {
    this.getApps();
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      userName: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      myApplication: ['']
    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    const userData = this.registerForm.value;
    const appData = this.apps.find(i => i.applicationUrl === userData.myApplication);
    userData.myApplication = appData;
    this.userService.registerAdmin(userData)
      .pipe(first())
      .subscribe(
        data => {
          if (data.status === 200) {
            this.alertService.success('Registration successful', true);
            this.loading = false;
          } else {
            this.alertService.error('User exists');
            this.loading = false;
          }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  getApps() {
    this.applicationService.getApps()
      .subscribe(apps => {
        this.apps = apps;
      }, error1 => {
        console.log('errori esht' + error1);
      });
  }
}
