import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {Router} from "@angular/router";
import {Application} from "../../_models/Application";
import {ApplicationServiceService} from "../../_services/application-service.service";
import {EditApplicationComponent} from "../edit-application/edit-application.component";

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.css']
})
export class ApplicationListComponent implements OnInit {


  displayedColumns: string[] = ['id', 'applicationUrl', 'applicationName'];
  dataSource: MatTableDataSource<Application>;
  applications: Application[] = [];
  dataToSend: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dialogRef: any;

  constructor(private applicationService: ApplicationServiceService, private router: Router, public dialog: MatDialog) {

  }

  ngOnInit(): void {
    this.getData();
  }

  showEditUser(app: Application) {

    this.dataToSend = app;
    this.dialogRef = this.dialog.open(EditApplicationComponent, {
      width: '550px',
      data: this.dataToSend
    });


  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getData() {
    this.applicationService.getAll().subscribe(
      applications => {
        this.applications = applications;
        this.dataSource = new MatTableDataSource(applications);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      }
    );
  }


}
