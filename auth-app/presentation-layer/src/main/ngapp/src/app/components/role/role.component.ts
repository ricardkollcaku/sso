import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AlertService, RoleService} from '../../_services/index';
import {MAT_DIALOG_DATA} from "@angular/material";
import {Application} from "../../_models/Application";

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {

  roleForm: FormGroup;
  submitted = false;
  application: Application;

  constructor(private formBuilder: FormBuilder,
              private roleService: RoleService,
              private alertService: AlertService,
              @Inject(MAT_DIALOG_DATA) private data: Application) {
    this.application = data;
  }

  get f() {
    return this.roleForm.controls;
  }

  ngOnInit() {
    this.roleForm = this.formBuilder.group({
      applicationUrl: this.application.applicationUrl,
      roleName: ''
    });

  }

  onSubmit() {
    this.submitted = true;
    this.roleService.save(this.roleForm.value)
      .subscribe(
        data => {
          this.alertService.success('Added successful', true);
        },
        error => {
          this.alertService.error(error);
        }
      );
  }

}
