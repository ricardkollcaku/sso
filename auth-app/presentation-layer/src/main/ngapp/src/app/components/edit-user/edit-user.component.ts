import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, UserService} from '../../_services/index';
import {UpdateUser} from '../../_models/updateUser';
import {ApplicationServiceService} from '../../_services/application-service.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatCheckboxChange} from "@angular/material";
import {Role} from "../../_models/role";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  userEditForm: FormGroup;
  currentUser: any;
  loading = false;
  submitted = false;
  apps;
  selectedAplication;
  selectedRoles: any;
  selectedUserROles: any;
  updatedUser: UpdateUser;

  // roles;


  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private userService: UserService,
              private applicationService: ApplicationServiceService,
              private alertService: AlertService,
              public dialogRef: MatDialogRef<EditUserComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.currentUser = data;
  }

  get f() {
    return this.userEditForm.controls;
  }

  ngOnInit() {

    this.getApps();
    this.getUserData();
  }

  getApps() {
    const user = JSON.parse(localStorage.getItem('userData'));
    if (user.systemRole === 'applicationAdmin') {
      this.apps = [user.myApplication];
    } else {
      this.applicationService.getApps()
        .subscribe(apps => {
          this.apps = apps;
        }, error1 => {
          console.log('errori esht' + error1);
        });
    }
  }

  getUserData() {
    this.initForm();
  }

  initForm() {
    this.userEditForm = this.formBuilder.group({
      name: [this.currentUser.name, Validators.required],
      applications: [''],
      roles: [''],
      active: [this.currentUser.active]
    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.userEditForm.invalid) {
      return;
    }
    this.loading = true;
    this.updatedUser = this.userEditForm.value;
    this.updatedUser.roles = this.selectedUserROles;
    this.updatedUser.email = this.currentUser.email;
    this.userService.update(this.updatedUser)
      .subscribe(
        data => {
          this.alertService.success('Update successful', true);
          this.dialogRef.close();
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
  }

  performSelect() {
    const that = this;
    this.selectedAplication = this.userEditForm.value.applications;
    this.apps.forEach(function (value) {
      if (value.applicationUrl === that.selectedAplication) {
        that.selectedRoles = value.roles;
        console.log(that.selectedRoles);

      }
    });
    that.getUser();

  }

  getUser() {
    const that = this;
    that.currentUser.applications.forEach(function (data) {
      if (data.applicationUrl === that.selectedAplication) {
        that.selectedUserROles = data.roles;
      }
    });
  }

  performChecked(role: Role, e: MatCheckboxChange) {
    console.log(e);
    if (e.checked) {
      this.selectedUserROles.push(role);
    } else {
      this.selectedUserROles.splice(this.selectedUserROles.indexOf(role), 1);
    }
  }
  cancel(){
    this.dialogRef.close();
  }
}
