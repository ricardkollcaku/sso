import {Component, OnInit} from '@angular/core';
import {AuthenticationService, UserService} from '../../_services';
import {Router} from '@angular/router';
import {ApplicationServiceService} from '../../_services/application-service.service';
import {User} from '../../_models';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {
  currentUser: User;
  token;
  apps;

  constructor(private userService: UserService,
              private authenticationService: AuthenticationService,
              private router: Router,
              private applicationService: ApplicationServiceService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = localStorage.getItem('currentUser');
  }

  ngOnInit() {
    this.getApps();
  }

  getApps() {
    this.applicationService.getApps()
      .subscribe(apps => {
        this.apps = apps;
        console.log(apps);
      }, error1 => {
        console.log('errori esht' + error1);
      });
  }

  logout() {
    this.authenticationService.logout().subscribe(data => {
        localStorage.removeItem('currentUser');
        this.router.navigate(['login']);
      },
      error => {
      },
    );
  }

}
