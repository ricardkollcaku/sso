﻿import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first, map} from 'rxjs/operators';

import {AlertService, AuthenticationService} from '../../_services/index';


@Component({
   templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
  }
)
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  homepage;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService) {
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  ngOnInit() {
    if (this.authenticationService.isAuthenticated()) {
      this.router.navigate(['/home']);
    }
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

    // reset login status
    //  this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['url'] || '';
    console.log("====================> redirect url: "+this.returnUrl)
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.authenticationService.login(this.f.email.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log(data);
          if (data.token) {
            // set the user
            this.homepage = this.authenticationService.getUser().pipe(map(res => {
              localStorage.setItem('userData', JSON.stringify(res))
              return "/";
            }));

            if (this.returnUrl) {
              const token = localStorage.getItem('currentUser');
              window.location.href = this.returnUrl + '/?token=' + token;
            } else {
              this.homepage.pipe(map(value => {
                this.router.navigate([value]);
                return value;
              })).subscribe();
            }
          } else {
            this.alertService.error('Invalid credentials');
            this.loading = false;
          }
        },
        error => {
          this.alertService.error('Invalid credentials');
          this.loading = false;
        });
  }
  keyDownFunction(event) {
    if(event.keyCode === 13) {
     this.onSubmit();
    }
  }
}
