import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApplicationServiceService} from '../../_services/application-service.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {
  applicationForm: FormGroup;
  validMessage: string = '';
  response;

  constructor(private applicationService: ApplicationServiceService) {

  }

  ngOnInit() {
    this.initForm();
  }

  submitRegistration() {
    this.applicationService.saveNewApplication(this.applicationForm.value).subscribe(value => {
        this.response = value;
        if (this.response.status === 200) {
          this.validMessage = 'done';
        }
      }
      ,
      err => {
        Observable.throw(err);
      }
    )
    ;
  }

  private initForm() {
    this.applicationForm = new FormGroup({
      applicationName: new FormControl('', Validators.required),
      applicationUrl: new FormControl('', Validators.required),
      logoutUrl: new FormControl('', Validators.required)
    });
  }
}
