import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../_services';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-refresh-token',
  templateUrl: './refresh-token.component.html',
  styleUrls: ['./refresh-token.component.css']
})
export class RefreshTokenComponent implements OnInit {

  private returnUrl: string;

  constructor(private authService: AuthenticationService,
              private router: Router,
              private route: ActivatedRoute) {
    this.returnUrl = this.route.snapshot.queryParams['url'] || '';
  }

  ngOnInit() {
    if (this.authService.getToken()) {
      if (this.authService.isTokenExpired(this.authService.getToken())) {
        this.authService.refreshToken(this.returnUrl).pipe(map(value => {
          if (value) {
            window.location.href = this.returnUrl + '?token=' + this.authService.getToken();
          } else {

            localStorage.clear();
            this.router.navigate(['/login'], {queryParams: {url: this.returnUrl}});
          }
        })).subscribe();

      } else {
        window.location.href = this.returnUrl + '?token=' + this.authService.getToken();
      }
    } else {
      this.router.navigate(['/login'], {queryParams: {url: this.returnUrl}});
    }
  }

}
