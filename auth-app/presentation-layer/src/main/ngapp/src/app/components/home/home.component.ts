﻿import {Component, OnInit} from '@angular/core';

import {User} from '../../_models/index';
import {AuthenticationService, UserService} from '../../_services/index';
import {Router} from '@angular/router';
import {ApplicationServiceService} from '../../_services/application-service.service';
import {MatDialog} from "@angular/material";
import {EditApplicationComponent} from "../edit-application/edit-application.component";
import {RoleComponent} from "../role/role.component";
import {Application} from "../../_models/Application";
import {ApplicationComponent} from "../application/application.component";

@Component({templateUrl: 'home.component.html', styleUrls: ['home.component.css']})
export class HomeComponent implements OnInit {
  currentUser: User;
  token;
  apps;
  breakpoint: number;

  constructor(private userService: UserService, private authenticationService: AuthenticationService, private router: Router,
              private applicationService: ApplicationServiceService, private dialog: MatDialog) {
    this.currentUser = JSON.parse(localStorage.getItem('userData'));
    this.token = localStorage.getItem('currentUser');
  }

  ngOnInit() {
    this.getApps();
    this.breakpoint = (window.innerWidth <= 1000) ? 1 : 4;
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 1000) ? 1 : 4;
  }

  getApps() {
    this.applicationService.getApps()
      .subscribe(apps => {
        this.apps = apps;
        console.log(apps);
      }, error1 => {
        console.log('errori esht' + error1);
      });
  }

  logout() {
    this.authenticationService.logout().subscribe(data => {
        localStorage.removeItem('currentUser');
        this.router.navigate(['login']);
      },
      error => {
      },
    );
  }

  addRole(application: Application) {
    this.dialog.open(RoleComponent, {
      width: '550px',
      data: application
    });
  }

  createApplication() {
    this.dialog.open(ApplicationComponent, {
      width: '550px'
    });
  }

  editApplication(application: Application) {
    this.dialog.open(EditApplicationComponent, {
      width: '550px',
      data: application
    });
  }

  isAdministrator(): boolean {
    return this.currentUser.systemRole === 'admin';
  }
}
