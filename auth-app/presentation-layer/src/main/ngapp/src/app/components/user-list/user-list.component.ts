import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../../_services';
import {UserDto} from '../../_models/userDto';
import {Router} from '@angular/router';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {EditUserComponent} from '../edit-user/edit-user.component';
import {AddUserComponent} from "../add-user/add-user.component";
import {User} from "../../_models";

import {of as observableOf} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators/catchError';
import {map} from 'rxjs/operators/map';
import {startWith} from 'rxjs/operators/startWith';
import {switchMap} from 'rxjs/operators/switchMap';
import {merge} from 'rxjs/observable/merge';
import {Page} from "../../_models/Page";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['id', 'name', 'email', 'actions'];
  dataSource: MatTableDataSource<UserDto> = new MatTableDataSource();
  users: UserDto[] = [];
  dataToSend: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dialogRef: any;
  resultsLength = 0;
  filters: Page;
  valueFilters: User = {
    id: null,
    name: '',
    lastName: '',
    email: '',
    active: null,
    systemRole: '',
    myApplication: ''
  }

  constructor(private userService: UserService, private router: Router, public dialog: MatDialog) {

  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.getData();
  }

  showEditUser(id: String) {

    this.userService.getById(id).subscribe(
      data => {
        this.dataToSend = data;
        this.dialogRef = this.dialog.open(EditUserComponent, {
          width: '550px',
          data: this.dataToSend
        });
      },
      error1 => {
      },

      () => {
        this.dialogRef.afterClosed().subscribe(result => {
          this.getData();
        });
      }
    );

  }

  cleanOBJ(obj) {
    let obj1: any = {};
    for (let propName of Object.keys(obj)) {
      if (!(obj[propName] === null || obj[propName] === undefined || obj[propName] === '')) {
        obj1[propName] = obj[propName];
      }
    }
    return obj1;
  }

  getData() {

    this.dataSource.sort = this.sort;
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge( this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          console.log(this.paginator.pageSize)
          this.filters = {
            needTotal: true,
            pageNumber: this.paginator.pageIndex,
            elementPerPage: this.paginator.pageSize,
            filter: this.cleanOBJ(this.valueFilters)
          };
          console.log(this.filters)
          // this.isLoadingResults = true;
          return this.userService.getAll(this.filters);
        }),
        map(data => {
          // this.isLoadingResults = false;
          // this.isRateLimitReached = false;
          this.resultsLength = data.totalElements;
          return data;

        }),
        catchError(() => {
          // this.isLoadingResults = false;
          // this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe((data: any) => {
        console.log(data)
      this.dataSource.data = data.content;
      console.log(this.dataSource.data);
    });
  }

  createUser() {
    this.dialog.open(AddUserComponent, {
      width: '500px'
    });
  }


}
