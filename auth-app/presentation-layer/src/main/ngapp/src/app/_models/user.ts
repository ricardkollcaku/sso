﻿export class User {
    id: number;
    email: string;
    name: string;
    lastName: string;
    active = true;
    systemRole: string;
    myApplication: string;
}
