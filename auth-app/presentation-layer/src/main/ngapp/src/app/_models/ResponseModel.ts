export interface ResponseModel {
    pageNumber?: number;
    pageSize?: number;
    totalElements?: number;
    content?: any[];
}