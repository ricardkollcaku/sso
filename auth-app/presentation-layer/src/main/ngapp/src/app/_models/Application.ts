export class Application {
  id: string;
  applicationUrl: string;
  applicationName: string;
  roles : string[];
  logoutUrl?: string;

}
