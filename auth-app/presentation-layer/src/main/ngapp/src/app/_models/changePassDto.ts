export class ChangePassDto {
  oldPass: string;
  newPass: string;
}
