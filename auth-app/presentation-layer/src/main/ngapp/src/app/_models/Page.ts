export interface Page {
    pageNumber?: number;
    elementPerPage?: number;
    needTotal?: boolean;
    filter?: string;
}