import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from '../_services';
import {map} from 'rxjs/operators';

@Injectable()
export class AdminGuard implements CanActivate {

  user: any;

  constructor(private router: Router, private authService: AuthenticationService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return this.authService.getUser().pipe(
      map(user => {
          this.user = user;
          if (this.user.systemRole === 'admin') {
            return true;
          }
          if (this.user.systemRole === 'user') {
            this.router.navigate(['user/home']);
          } else {
            this.router.navigate(['appAdmin/home']);
          }
          return false;
        }
      ));

  }
}
