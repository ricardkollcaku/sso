﻿import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from '../_services';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthenticationService) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!localStorage.getItem('currentUser')) {
      this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
      return false;
    }
    return true;
    // if (!this.authService.isTokenExpired()) {
    //   return true;
    // } else {
    //   return this.authService.refreshToken()
    //     .pipe(map(value => {
    //       if (!value) {
    //         this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    //       }
    //       return value;
    //     }));
    // }
    // localStorage.removeItem('currentUser');
    // not logged in so redirect to login page with the return url
  }
}
