﻿import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home';
import {RegisterComponent} from './components/register';
import {AuthGuard} from './_guards';
import {RoleComponent} from './components/role/role.component';
import {ApplicationComponent} from './components/application/application.component';
import {UserListComponent} from './components/user-list/user-list.component';
import {EditUserComponent} from './components/edit-user/edit-user.component';
import {AddUserComponent} from './components/add-user/add-user.component';
import {AdminGuard} from './_guards/admin.guard';
import {AddAdminComponent} from './components/add-admin/add-admin.component';
import {AdminHomeComponent} from './components/admin-home/admin-home.component';
import {AppAdminHomeComponent} from './components/app-admin-home/app-admin-home.component';
import {RefreshTokenComponent} from './components/refresh-token/refresh-token.component';
import {LoginComponent} from './components/login/login.component';
import {ApplicationListComponent} from "./components/application-list/application-list.component";

const appRoutes: Routes = [
  {path: '', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'user/home', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'admin/home', component: AdminHomeComponent, canActivate: [AuthGuard, AdminGuard]},
  {path: 'appAdmin/home', component: AppAdminHomeComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'role', component: RoleComponent, canActivate: [AuthGuard, AdminGuard]},
  {path: 'applications', component: ApplicationListComponent, canActivate: [AuthGuard, AdminGuard]},
  {path: 'users', component: UserListComponent, canActivate: [AuthGuard]},
  {path: 'editUser/:id', component: EditUserComponent, canActivate: [AuthGuard]},
  {path: 'addUser', component: AddUserComponent, canActivate: [AuthGuard]},
  {path: 'addAdmin', component: AddAdminComponent, canActivate: [AuthGuard, AdminGuard]},
  {path: 'login/refresh', component: RefreshTokenComponent},
  {path: '**', redirectTo: ''}
];

export const routing = RouterModule.forRoot(appRoutes);
