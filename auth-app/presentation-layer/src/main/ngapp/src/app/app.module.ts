﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {routing} from './app.routing';
import {AlertComponent} from './_directives';
import {AuthGuard} from './_guards';
import {ErrorInterceptor, JwtInterceptor} from './_helpers';
import {AlertService, AuthenticationService, RoleService, UserService} from './_services';
import {HomeComponent} from './components/home';

import {RegisterComponent} from './components/register';
import {RoleComponent} from './components/role/role.component';
import {ApplicationComponent} from './components/application/application.component';
import {HeaderComponent} from './components/header/header.component';
import {UserListComponent} from './components/user-list/user-list.component';
import {EditUserComponent} from './components/edit-user/edit-user.component';
import {AddUserComponent} from './components/add-user/add-user.component';
import {AdminGuard} from './_guards/admin.guard';
import {AddAdminComponent} from './components/add-admin/add-admin.component';
import {AdminHomeComponent} from './components/admin-home/admin-home.component';
import {AppAdminHomeComponent} from './components/app-admin-home/app-admin-home.component';
import {RefreshTokenComponent} from './components/refresh-token/refresh-token.component';
import {MaterialModule} from './material.module';
import {LoginComponent} from './components/login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ChangePassComponent} from './components/change-pass/change-pass.component';
import {ApplicationListComponent} from './components/application-list/application-list.component';
import {EditApplicationComponent} from './components/edit-application/edit-application.component'

  ;


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    routing,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
  ],
  declarations: [
    AppComponent,
    AlertComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    RoleComponent,
    ApplicationComponent,
    HeaderComponent,
    UserListComponent,
    EditUserComponent,
    AddUserComponent,
    AddAdminComponent,
    AppAdminHomeComponent,
    AdminHomeComponent,
    RefreshTokenComponent,
    ChangePassComponent,
    ApplicationListComponent,
    EditApplicationComponent
  ],


  entryComponents: [
    ChangePassComponent,
    EditApplicationComponent,
    ApplicationComponent
  ],

  providers:
    [
      AuthGuard,
      AdminGuard,
      AlertService,
      AuthenticationService,
      UserService,
      RoleService,
      {
        provide: MatDialogRef,
        useValue: {}
      }, {
      provide: MAT_DIALOG_DATA,
      useValue: {} // Add any data you wish to test if it is passed/used correctly
    },
      {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
      {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
    ],
  bootstrap:
    [AppComponent]
})

export class AppModule {
}
