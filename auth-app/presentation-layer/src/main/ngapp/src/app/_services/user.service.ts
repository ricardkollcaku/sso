﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../_models';
import {environment} from '../../environments/environment';
import {UpdateUser} from '../_models/updateUser';
import {ChangePassDto} from "../_models/changePassDto";
import {ResponseModel} from "../_models/ResponseModel";
import {Page} from "../_models/Page";

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {
  }


  getAll(filters: Page) {
    let a: string = JSON.stringify(filters.filter);
    a = a.replace('}', '')
    a = a.replace('{', '')
    const url = environment.apiUrl +
      '/users/applicationUsers/filtered?pageNumber=' +
      filters.pageNumber + '&elementPerPage=' + filters.elementPerPage +
      '&needTotal=' + filters.needTotal+
      '&filters='+a;
    return this.http.get<ResponseModel>(url);
  }

  getById(id: String) {
    return this.http.get(`${environment.apiUrl}/users/user/` + id);
  }

  adminRegister(user: User) { 
    return this.http.post<any>(`${environment.apiUrl}/admin/register`, user);
  }

  register(user: User) {
    return this.http.post<any>(`${environment.apiUrl}/users/register`, user);
  }

  changePass(changePassDto: ChangePassDto) {
    return this.http.post<any>(`${environment.apiUrl}/users/changePass`, changePassDto);
  }


  registerAdmin(user: User) {
    return this.http.post<any>(`${environment.apiUrl}/admin/registerAppAdmin`, user);
  }

  update(user: UpdateUser) {

    console.log(user);
    return this.http.put(`${environment.apiUrl}/users/update`, user);
  }

  checkAdmin() {
    return this.http.get(`${environment.apiUrl}/users/hasAdmin`);
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiUrl}/users/` + id);
  }
}
