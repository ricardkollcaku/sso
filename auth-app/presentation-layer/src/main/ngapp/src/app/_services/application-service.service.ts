import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from "rxjs";
import {Application} from "../_models/Application";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ApplicationServiceService {

  constructor(private http: HttpClient) {
  }

  saveNewApplication(application) {
    const body = JSON.stringify(application);
    return this.http.post(`${environment.apiUrl}/applications/save`, body, httpOptions);
  }

  getApps() {
    return this.http.get(`${environment.apiUrl}/applications/all`);
  }
  getAll():Observable<Application[]> {
    return this.http.get<Application[]>(`${environment.apiUrl}/applications/all`);
  }


  editApplication(myApplication: Application) {
    const body = JSON.stringify(myApplication);
    return this.http.put(`${environment.apiUrl}/applications/update`, body, httpOptions);
  }
}
