﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import * as jwt_decode from 'jwt-decode';
import {Router} from '@angular/router';

@Injectable()
export class AuthenticationService {

  user: any;

  constructor(private http: HttpClient, private router: Router) {
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/users/login`, {email: username, password: password})
      .pipe(map(user => {
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      }));
  }

  getUserDetails() {
    return this.user;
  }

  getUser() {
    const url = environment.apiUrl + '/users/signedIn';
    return this.http.get<any>(url);
  }

  logout() {
    // remove user from local storage to log user out
    this.user = null;
    return this.http.get(`${environment.apiUrl}/logout`);
  }

  isAuthenticated(): boolean {
    return this.getToken() != null;
  }

  getToken(): string {
    return localStorage.getItem('currentUser');
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) {
      return null;
    }
    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if (!token) {
      token = this.getToken();
    }
    if (!token) {
      return true;
    }

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) {
      return false;
    }
    return !(date.valueOf() > new Date().valueOf());
  }

  refreshToken(returnUrl: string) {
    const url = environment.apiUrl + '/refreshToken';
    return this.http.get<any>(url).pipe(
      map(user => {
        if (user && user.token !== 'null') {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          return true;
        } else {
          localStorage.removeItem('currentUser');
          this.logout();
          if (returnUrl) {
            window.location.href = returnUrl;
          } else {
            this.router.navigate(['/login']);
          }

          return false;
        }
      })
    );
  }

  refreshAppToken() {
    const url = environment.apiUrl + '/refreshToken';
    return this.http.get<any>(url).pipe(
      map(user => {
        if (user && user.token !== 'null') {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          return true;
        } else {
          localStorage.removeItem('currentUser');
          this.logout();
          return false;
        }
      })
    );
  }


}
