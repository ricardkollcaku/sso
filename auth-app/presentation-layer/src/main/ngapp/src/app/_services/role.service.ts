import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Role} from '../_models/role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  constructor(private http: HttpClient) {
  }

  save(role: Role) {
    return this.http.post(`${environment.apiUrl}/roles/save`, role);
  }

}
