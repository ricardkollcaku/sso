﻿import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {environment} from '../environments/environment';

@Component({
  selector: 'app',
  templateUrl: 'app.component.html'
})

export class AppComponent {
  private serverUrl: string;
  private token;

  constructor(private router: Router) {
    this.token = JSON.parse(localStorage.getItem('currentUser'));
    this.serverUrl = environment.socketEndpoint;
    this.initializeWebSocketConnection();

  }

  initializeWebSocketConnection() {
    const that = this;
    const socket = new WebSocket(this.serverUrl);
    socket.addEventListener('open', function (event) {
      socket.send('Hello Server!');
    });
    socket.addEventListener('message', function (text) {
      const a = JSON.parse(localStorage.getItem('currentUser'));
      if (a === null) {
        that.router.navigate(['login']);
      } else if (text.data === a.token) {
        console.log(text.data);
        localStorage.removeItem('currentUser');
        that.router.navigate(['login']);
      }
    });

  }

}
