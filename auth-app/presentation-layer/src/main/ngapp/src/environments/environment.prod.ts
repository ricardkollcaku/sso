export const environment = {
  production: true,
  apiUrl: 'http://54.37.81.35:3030/api/v1',
  socketEndpoint: 'ws://54.37.81.35:3030/socket',
  systemRole: 'admin'
};
