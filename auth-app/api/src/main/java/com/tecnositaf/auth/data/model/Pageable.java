package com.tecnositaf.auth.data.model;

public class Pageable {
    private Integer pageNumber;
    private Integer elementPerPage;
    private boolean needTotal = true;

    public Pageable(Integer pageNumber, Integer elementPerPage, boolean needTotal) {
        this.pageNumber = pageNumber;
        this.elementPerPage = elementPerPage;
        this.needTotal = needTotal;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getElementPerPage() {
        return elementPerPage;
    }

    public void setElementPerPage(Integer elementPerPage) {
        this.elementPerPage = elementPerPage;
    }

    public boolean isNeedTotal() {
        return needTotal;
    }

    public void setNeedTotal(boolean needTotal) {
        this.needTotal = needTotal;
    }
}
