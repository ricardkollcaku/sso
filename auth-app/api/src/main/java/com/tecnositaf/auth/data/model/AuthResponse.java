package com.tecnositaf.auth.data.model;

import com.tecnositaf.auth.data.dto.UserDto;

public class AuthResponse {
    private Boolean validated;
    private UserDto user;

    public Boolean getValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}
