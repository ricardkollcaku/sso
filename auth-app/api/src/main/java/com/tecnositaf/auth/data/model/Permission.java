package com.tecnositaf.auth.data.model;

import java.util.LinkedHashMap;

public class Permission {
    private LinkedHashMap<String, String> permission;

    public LinkedHashMap<String, String> getPermission() {
        return permission;
    }

    public void setPermission(LinkedHashMap<String, String> permission) {
        this.permission = permission;
    }
}
