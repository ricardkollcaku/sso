package com.tecnositaf.auth.data.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.ArrayList;

public class User {
    @Id
    private String id;
    private String name;
    @Indexed(unique = true, sparse = true)
    private String email;
    @Indexed(unique = true, sparse = true)
    private String userName;
    private String lastName;
    private String password;
    private String token;
    private boolean active = true;
    private String systemRole = "user";
    private Applications myApplication;
    private ArrayList<Applications> applications;
    private long lastRequest;
    private boolean ldapUser = false;

    public boolean isLdapUser() {
        return ldapUser;
    }

    public void setLdapUser(boolean ldapUser) {
        this.ldapUser = ldapUser;
    }

    public long getLastRequest() {
        return lastRequest;
    }

    public void setLastRequest(long lastRequest) {
        this.lastRequest = lastRequest;
    }

    public Applications getMyApplication() {
        return myApplication;
    }

    public void setMyApplication(Applications myApplication) {
        this.myApplication = myApplication;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public ArrayList<Applications> getApplications() {
        return applications;
    }

    public void setApplications(ArrayList<Applications> applications) {
        this.applications = applications;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSystemRole() {
        return systemRole;
    }

    public void setSystemRole(String systemRole) {
        this.systemRole = systemRole;
    }
}
