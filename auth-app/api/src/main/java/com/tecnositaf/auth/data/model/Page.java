package com.tecnositaf.auth.data.model;

import java.util.List;


public class Page<T> {

    private static final String FIRST_PAGE_NUM = "0";
    private static final String DEFAULT_PAGE_SIZE = "20";

    private List<T> content;
    private int pageNumber;
    private int pageSize;
    private long totalElements;

    public Page() {
    }

    public static String getFirstPageNum() {
        return FIRST_PAGE_NUM;
    }

    public static String getDefaultPageSize() {
        return DEFAULT_PAGE_SIZE;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }
}
