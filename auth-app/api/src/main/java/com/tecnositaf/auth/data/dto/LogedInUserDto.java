package com.tecnositaf.auth.data.dto;

import com.tecnositaf.auth.data.model.Applications;

public class LogedInUserDto {
    private String id;
    private String name;
    private String email;
    private String userName;
    private String lastName;
    private String systemRole;
    private Applications myApplication;

    public Applications getMyApplication() {
        return myApplication;
    }

    public void setMyApplication(Applications myApplication) {
        this.myApplication = myApplication;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSystemRole() {
        return systemRole;
    }

    public void setSystemRole(String systemRole) {
        this.systemRole = systemRole;
    }
}
