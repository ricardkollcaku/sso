package com.tecnositaf.auth.data.dto;


import org.springframework.http.HttpStatus;

import java.util.List;

public class UsersDtoResponseObject {

    private List<UserDto> body;

    public UsersDtoResponseObject(HttpStatus status, List<UserDto> body) {
        this.body = body;
    }

    public UsersDtoResponseObject() {
    }



    public List<UserDto> getBody() {
        return body;
    }

    public void setBody(List<UserDto> body) {
        this.body = body;
    }
}
