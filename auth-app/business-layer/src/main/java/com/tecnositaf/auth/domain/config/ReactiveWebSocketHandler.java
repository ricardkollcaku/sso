package com.tecnositaf.auth.domain.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;

@Component("ReactiveWebSocketHandler")
public class ReactiveWebSocketHandler implements WebSocketHandler {
    WebSocketSession webSocketSession;
    @Autowired
    SocketMessageComponent socketMessageComponent;


    @Override
    public Mono<Void> handle(WebSocketSession webSocketSession) {
        this.webSocketSession = webSocketSession;
        return webSocketSession.send(socketMessageComponent.getSubscriber()
                .map(webSocketSession::textMessage))
                .and(webSocketSession.receive()
                        .map(WebSocketMessage::getPayloadAsText).log());
    }

}
