package com.tecnositaf.auth.domain.config;

import org.springframework.stereotype.Component;
import reactor.core.publisher.DirectProcessor;
import reactor.core.publisher.Flux;

@Component
public class SocketMessageComponent {
    private DirectProcessor<String> emitterProcessor;
    private Flux<String> subscriber;

    public SocketMessageComponent() {
        emitterProcessor = DirectProcessor.create();
        subscriber = emitterProcessor.share();
    }

    public Flux<String> getSubscriber() {
        return subscriber;
    }

    public void sendMessage(String mesage) {
        emitterProcessor.onNext(mesage);
    }

}
