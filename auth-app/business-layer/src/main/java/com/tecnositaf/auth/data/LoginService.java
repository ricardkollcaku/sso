package com.tecnositaf.auth.data;

import com.tecnositaf.auth.data.dto.UserDto;
import reactor.core.publisher.Mono;

public interface LoginService {
    public Mono<String> authenticateUser(String username, String password);
}
