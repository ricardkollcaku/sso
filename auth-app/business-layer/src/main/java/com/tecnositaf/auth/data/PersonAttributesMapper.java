package com.tecnositaf.auth.data;


import org.springframework.ldap.core.AttributesMapper;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

public class PersonAttributesMapper implements AttributesMapper {

    @Override
    public Object mapFromAttributes(Attributes attributes) throws NamingException {
        Person person = new Person();


        Attribute serialNumber = attributes.get("serialnumber");
        if (serialNumber != null) {
            person.setSerialNumber((String) serialNumber.get());
        }

        Attribute userPrincipalName = attributes.get("userprincipalname");
        if (userPrincipalName != null) {
            person.setUserPrincipalName((String) userPrincipalName.get());
        }

        Attribute streetAddress = attributes.get("streetaddress");
        if (streetAddress != null) {
            person.setStreetAddress((String) streetAddress.get());
        }

        Attribute mobile = attributes.get("mobile");
        if (mobile != null) {
            person.setMobile((String) mobile.get());
        }

        Attribute name = attributes.get("name");
        if (name != null) {
            person.setName((String) name.get());
        }

        Attribute ou = attributes.get("ou");
        if (ou != null) {
            person.setOrganisationUnit((String) ou.get());
        }

        Attribute username = attributes.get("cn");
        if (username != null) {
            person.setUserName((String) username.get());
        }

        Attribute displayname = attributes.get("displayname");
        if (displayname != null) {
            person.setDisplayName((String) displayname.get());
        }

        Attribute lastname = attributes.get("sn");
        if (lastname != null) {
            person.setLastName((String) lastname.get());
        }

        Attribute firstname = attributes.get("givenname");
        if (firstname != null) {
            person.setFirstName((String) firstname.get());
        }

        Attribute mail = attributes.get("mail");
        if (mail != null) {
            person.setMail((String) mail.get());
        }

        Attribute userid = attributes.get("sAMAccountName");

        if (userid != null) {
            person.setUserID((String) userid.get());
        }
     //   System.out.println(attributes.toString());
        return person;
    }
}