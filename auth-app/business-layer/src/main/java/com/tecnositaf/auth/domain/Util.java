package com.tecnositaf.auth.domain;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class Util {
    public HashMap<String, Object> parseStringToHashmap(String json) {
        if (json== null || json.length()==0 || json.equals("null"))
            return null;
        if (json != null && json.length() > 0 && json.charAt(0) != '{')
            json = "{" + json + "}";
        HashMap<String, Object> map = new HashMap<>();
        try {
            ObjectMapper mapper = new ObjectMapper();

            map = mapper.readValue(json, new TypeReference<Map<String, Object>>() {
            });
            System.out.println(map);

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map.size() > 0 ? map : null;
    }


}
