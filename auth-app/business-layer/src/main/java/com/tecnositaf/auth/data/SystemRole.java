package com.tecnositaf.auth.data;

public class SystemRole {
    public static String ADMIN = "admin";
    public static String USER = "user";
    public static String APPLICATION_ADMIN = "applicationAdmin";

}
