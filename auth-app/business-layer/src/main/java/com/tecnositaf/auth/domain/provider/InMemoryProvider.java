package com.tecnositaf.auth.domain.provider;

import com.tecnositaf.auth.data.model.User;
import com.tecnositaf.auth.domain.security.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.HashMap;

@Component
public class InMemoryProvider {
    public static HashMap<String, User> userHashMap;
    @Autowired
    TokenProvider tokenProvider;
    @Value("${security.jwt.token.expire-length}")
    private long validityInMilliseconds;

    @PostConstruct
    public void initMap() {
        userHashMap = new HashMap<>();
        Flux.interval(Duration.ofHours(10))
                .map(aLong -> {
                    System.out.println("running cleaner");
                    return aLong;
                })
                .flatMapIterable(aLong -> userHashMap.entrySet())
                .flatMap(stringUserEntry -> {
                    if ((stringUserEntry.getValue().getLastRequest() + validityInMilliseconds) > System.currentTimeMillis())
                        return Mono.empty();
                    return Mono.just(stringUserEntry);
                })
                .map(stringUserEntry -> {
                    remove(stringUserEntry.getKey());
                    return stringUserEntry;
                })
                .subscribeOn(Schedulers.newSingle("timer"))
                .subscribe();

    }

    public void remove(String email) {
        userHashMap.remove(email);
    }

    public User getUser(String email) {
        System.out.println("getting user" + userHashMap.get(email));
        return userHashMap.get(email);

    }


    public void save(User user) {
        System.out.println("saving user");
        userHashMap.put(user.getEmail(), user);
    }

}
