package com.tecnositaf.auth.controller;

import com.tecnositaf.auth.data.dto.RoleDto;
import com.tecnositaf.auth.data.model.Applications;
import com.tecnositaf.auth.domain.provider.ApplicationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
public class ApplicationController {
    @Autowired
    ApplicationProvider applicationProvider;

    @CrossOrigin
    @GetMapping(value = "/api/v1/applications/all")
    public Mono<ResponseEntity<List<Applications>>> getApplicationList() {
        return applicationProvider.getAllApplications()
                .collectList()
                .map(applications -> new ResponseEntity<>(applications, HttpStatus.OK));
    }

    @CrossOrigin
    @PreAuthorize("hasAuthority('admin')")
    @PostMapping(value = "/api/v1/applications/save")
    public Mono<ResponseEntity> createApplication(@RequestBody Applications applications) {
        return applicationProvider.createApplication(applications);
    }

    @CrossOrigin
    @PreAuthorize("hasAnyAuthority('admin','applicationAdmin')")
    @PostMapping(value = "/api/v1/roles/save")
    public Mono<ResponseEntity> addRoleToApplication(@RequestBody RoleDto roleDto) {
        return applicationProvider.addRoleToApplication(roleDto);
    }

    @CrossOrigin
    @PreAuthorize("hasAnyAuthority('admin','applicationAdmin')")
    @PutMapping(value = "/api/v1/applications/update")
    public Mono<ResponseEntity> updateUser(@RequestBody Applications user) {
        return applicationProvider.updateApplication(user)
                .map(aVoid -> ResponseEntity.ok().build());
    }
}
