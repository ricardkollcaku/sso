package com.tecnositaf.auth.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tecnositaf.auth.data.LoginService;
import com.tecnositaf.auth.data.Strings;
import com.tecnositaf.auth.data.SystemRole;
import com.tecnositaf.auth.data.dto.*;
import com.tecnositaf.auth.data.model.AuthResponse;
import com.tecnositaf.auth.data.model.Page;
import com.tecnositaf.auth.data.model.Pageable;
import com.tecnositaf.auth.data.model.User;
import com.tecnositaf.auth.domain.provider.UserProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserProvider userProvider;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    LoginService loginService;

    @CrossOrigin
    @GetMapping(value = "/api/v1/users/hasAdmin")
    public Mono<ResponseEntity<Boolean>> isAdminInserted() {
        return userProvider.getUserByRole(SystemRole.ADMIN)
                //.then(Mono.just(new ResponseEntity<>(true, HttpStatus.OK)))
                .map(user -> new ResponseEntity<>(true, HttpStatus.OK))
                .switchIfEmpty(Mono.just(new ResponseEntity<>(false, HttpStatus.OK)));

    }

    @CrossOrigin
    @GetMapping(value = "/api/v1/users/all")
    public Mono<ResponseEntity<List<SimpleUserDto>>> getUserList(ServerHttpRequest httpServletRequest) {
        return userProvider.getAllUsers(httpServletRequest)
                .collectList()
                .map(simpleUserDto -> new ResponseEntity<>(simpleUserDto, HttpStatus.OK));
    }


    @CrossOrigin
    @GetMapping(value = "/api/v1/users/user/{id}")
    public Mono<ResponseEntity<User>> getUser(@PathVariable("id") String id) {
        return userProvider.getUserById(id)
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK));

    }

    @CrossOrigin
    @PostMapping(value = "/api/v1/users/login")
    public Mono<ResponseEntity> login(@RequestBody LoginDto loginDto, ServerHttpRequest servletRequest) {

        return loginService.authenticateUser(loginDto.getEmail(), loginDto.getPassword())
                .map(s -> new ResponseEntity(new TokenDto(s), HttpStatus.OK))
                .switchIfEmpty(Mono.just(new ResponseEntity(Strings.LOGINERROR, HttpStatus.FORBIDDEN)));
    }


    @CrossOrigin
    @PostMapping(value = "/api/v1/users/changePass")
    public Mono<ResponseEntity<User>> changePass(@RequestBody ChangePassDto changePassDto, ServerHttpRequest serverHttpRequest) {
        return userProvider.changePass(serverHttpRequest, changePassDto)
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .switchIfEmpty(Mono.just(ResponseEntity.unprocessableEntity().build()));
    }

    @CrossOrigin
    @PostMapping(value = "/api/v1/admin/register")
    public Mono<ResponseEntity> createAdmin(@RequestBody User user) {
        System.out.println("u thrit " + user.getEmail());
        user.setActive(true);
        user.setSystemRole(SystemRole.ADMIN);
        return isAdminInserted()
                .flatMap(adminInserted -> {
                    if (adminInserted.getBody()) {
                        return Mono.just(new ResponseEntity<>(Strings.FORBIDEN, HttpStatus.FORBIDDEN));
                    } else {
                        userProvider.createUser(user).subscribeOn(Schedulers.parallel()).subscribe();
                        return Mono.just(new ResponseEntity<>("User created", HttpStatus.OK));
                    }
                });
    }

    @CrossOrigin
    @PostMapping(value = "/api/v1/admin/registerAppAdmin")
    public Mono<ResponseEntity> createAppAdmin(@RequestBody User user) {
        user.setSystemRole(SystemRole.APPLICATION_ADMIN);
        user.setActive(true);

        return userProvider.createUser(user);
    }

    @CrossOrigin
    @PostMapping(value = "/api/v1/users/register")
    public Mono<ResponseEntity> createUser(@RequestBody User user) {
        user.setActive(true);
        return userProvider.createUser(user);
    }

    @CrossOrigin
    @PutMapping(value = "/api/v1/users/update")
    public Mono<ResponseEntity> updateUser(@RequestBody ResponseUserDto user) {
        System.out.println("u thrit " + user.getEmail());
        return userProvider.updateUser(user);
    }

    @CrossOrigin
    @GetMapping(value = "/api/v1/users/signedIn")
    public Mono<ResponseEntity> getUserByToken(ServerHttpRequest servletRequest) {
        return userProvider.getUserByToken(servletRequest)
                .map(user -> userProvider.getFrontUserByUSer(user))
                .map(user -> new ResponseEntity(user, HttpStatus.OK))
                .switchIfEmpty(Mono.just(new ResponseEntity(Strings.FORBIDEN, HttpStatus.FORBIDDEN)));

    }

    @CrossOrigin
    @GetMapping(value = "/api/v1/users/applicationUsers")
    public Mono<UsersDtoResponseObject> getUsersFromApplication(ServerHttpRequest servletRequest) {
        return userProvider.getAllUsersByApplication(servletRequest)
                .collectList()
                .map(userDtos -> new UsersDtoResponseObject(HttpStatus.OK, userDtos));
    }

    @CrossOrigin
    @GetMapping(value = "/api/v1/users/applicationUsers/filtered")
    public Mono<Page<UserDto>> getUsers(@RequestParam Integer pageNumber, @RequestParam Integer elementPerPage, @RequestParam Boolean needTotal, @RequestParam(value = "filters") String filter, ServerHttpRequest servletRequest) {
        return userProvider.getAllUsersByApplicationPagable(new Pageable(pageNumber, elementPerPage, needTotal), servletRequest, filter);
    }

    @CrossOrigin
    @PostMapping(value = "/api/v1/users/all")
    public Mono<ResponseEntity<Page<SimpleUserDto>>> getUserList(ServerHttpRequest httpServletRequest, @RequestBody Pageable pageable) {
        return userProvider.getAllUserPagable(pageable, httpServletRequest)
                .map(simpleUserDto -> new ResponseEntity<>(simpleUserDto, HttpStatus.OK));
    }


    @CrossOrigin
    @GetMapping(value = "/api/v1/users/isUserPresent")
    public Mono<Boolean> isUserPresent(ServerHttpRequest httpServletRequest) {
        return userProvider.getUserByUsername(httpServletRequest.getHeaders().getFirst("username"))
                .map(user -> true)
                .switchIfEmpty(Mono.just(false));
    }

}