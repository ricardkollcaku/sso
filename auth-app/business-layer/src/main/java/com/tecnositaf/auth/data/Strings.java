package com.tecnositaf.auth.data;

public class Strings {
    public static String LOGINERROR = "Login failed";
    public static String REGISTERERROR = "Register failed";
    public static String FORBIDEN = "Forbiden";
}
