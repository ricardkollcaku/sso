package com.tecnositaf.auth.domain.provider;

import com.tecnositaf.auth.data.LoginService;
import com.tecnositaf.auth.data.Person;
import com.tecnositaf.auth.data.PersonAttributesMapper;
import com.tecnositaf.auth.data.SystemRole;
import com.tecnositaf.auth.data.model.Applications;
import com.tecnositaf.auth.data.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.ldap.query.SearchScope;
import org.springframework.ldap.support.LdapUtils;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LdapLoginProvider implements LoginService {
    @Autowired
    UserProvider userProvider;
    @Autowired
    LdapContextSource ldapContextSource;
    @Autowired
    LdapTemplate ldapTemplate;
    @Autowired
    ApplicationProvider applicationProvider;

    @Value("${sso.auth.provider.ldap.basedomainname}")
    String baseDomainName;

    @PostConstruct
    public void postConstruct() {
        ldapTemplate.setIgnorePartialResultException(true);
    }

    @Override
    public Mono<String> authenticateUser(String username, String password) {
        return loginLdap(username, password)
                .switchIfEmpty(userProvider.login(username, password));
    }

    private Mono<String> loginLdap(String username, String password) {
        if (authenticate(username, password))
            return getUserWithApplication(getPersonById(username))
                    .flatMap(this::saveUserIfNotExist)
                    .flatMap(userProvider::getToken);
        else return Mono.empty();
    }

    private boolean authenticate(String id, String password) {
        if (id == null || id.length() < 1 || password == null || password.length() < 1)
            return false;
        return ldapTemplate.authenticate("", "(&(objectclass=person)(sAMAccountName=" + id + "))", password);
    }

    private Person getPersonById(String id) {
        List<Person> list = ldapTemplate.search(getUserQueryById(id), new PersonAttributesMapper());
        if (list.size() == 1)
            return list.get(0);
        return null;
    }

    private LdapQuery getUserQueryById(String id) {
        return LdapQueryBuilder.query()
                .searchScope(SearchScope.SUBTREE)
                .timeLimit(3)
                .countLimit(1)
                .base(LdapUtils.emptyLdapName())
                .where("objectclass").is("person")
                .and("sAMAccountName").is(id);
    }

    private LdapQuery getUserQuers() {
        return LdapQueryBuilder.query()
                .searchScope(SearchScope.SUBTREE)
                .timeLimit(999999)
                .countLimit(1999999)
                .base(LdapUtils.emptyLdapName())
                .where("objectclass").is("person")
                .and("primaryGroupID").is("513");
    }

    private User getUserModel(Person person) {
        User user = new User();
        user.setUserName(person.getUserID());
        user.setName(person.getFirstName());
        user.setActive(true);
        user.setSystemRole(SystemRole.USER);
        user.setId(person.getUserID());
        user.setLastName(person.getLastName());
        user.setLdapUser(true);
        user.setEmail(person.getMail() != null ? person.getMail() : person.getUserPrincipalName() != null ? person.getUserPrincipalName() : person.getUserID());
        return user;
    }

    private Mono<User> saveUserIfNotExist(User user) {
        return userProvider.saveIfNotExist(user);
    }

    private Mono<User> getUserWithApplication(Person person) {
        return applicationProvider.getAllApplications()
                .collectList()
                .map(applications -> getUserModel(person, (ArrayList<Applications>) applications));
    }

    private User getUserModel(Person person, ArrayList<Applications> applications) {
        User user = getUserModel(person);
        applications = (ArrayList<Applications>) applications
                .stream()
                .map(applications1 -> {
                    applications1.setRoles(new ArrayList<>());
                    return applications1;
                })
                .collect(Collectors.toList());
        user.setApplications(applications);
        return user;
    }

}
