package com.tecnositaf.auth.data.repository;

import com.tecnositaf.auth.data.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepo extends MongoRepository<User, String> {
    User findByEmail(String email);

    User findByEmailAndActive(String email, Boolean active);

}
