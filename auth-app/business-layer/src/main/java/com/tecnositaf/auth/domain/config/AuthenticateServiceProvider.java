package com.tecnositaf.auth.domain.config;

import com.tecnositaf.auth.data.LoginService;
import com.tecnositaf.auth.domain.provider.LdapLoginProvider;
import com.tecnositaf.auth.domain.provider.MongoLoginProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

@Configuration
public class AuthenticateServiceProvider {
    @Value("${sso.auth.provider.ldap.enable}")
    Boolean ldap;
    @Value("${sso.auth.provider.ldap.server}")
    String server;
    @Value("${sso.auth.provider.ldap.user}")
    String user;
    @Value("${sso.auth.provider.ldap.password}")
    String password;
    @Value("${sso.auth.provider.ldap.basedomainname}")
    String baseDomainName;
    @Value("${sso.auth.provider.ldap.securityauth}")
    String securityAuth;

    @Bean
    public LoginService loginService() {
        return ldap ? new LdapLoginProvider() : new MongoLoginProvider();
    }

    @Bean
    public LdapContextSource contextSource() {
        if (ldap) {

            LdapContextSource contextSource = new LdapContextSource();
            contextSource.setUrl(server);
            contextSource.setBase(baseDomainName);
            contextSource.setUserDn(user);
            contextSource.setPassword(password);
          //  contextSource.setReferral("follow");

            return contextSource;
        } else return null;
    }

    @Bean
    public LdapTemplate ldapTemplate() {

        return ldap ? new LdapTemplate(contextSource()) : null;
    }

}
