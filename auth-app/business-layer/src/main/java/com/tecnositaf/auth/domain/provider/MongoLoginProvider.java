package com.tecnositaf.auth.domain.provider;

import com.tecnositaf.auth.data.LoginService;
import com.tecnositaf.auth.data.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Mono;

public class MongoLoginProvider implements LoginService {
    @Autowired
    UserProvider userProvider;
    @Override
    public Mono<String> authenticateUser(String username, String password) {
        return userProvider.login(username,password);
    }
}
