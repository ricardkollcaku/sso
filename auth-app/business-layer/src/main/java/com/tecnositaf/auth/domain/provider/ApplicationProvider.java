package com.tecnositaf.auth.domain.provider;

import com.tecnositaf.auth.data.dto.RoleDto;
import com.tecnositaf.auth.data.model.Applications;
import com.tecnositaf.auth.data.repository.ApplicationReactiveRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;

@Component
public class ApplicationProvider {

    @Autowired
    ApplicationReactiveRepo applicationReactiveRepo;

    @Autowired
    UserProvider userProvider;

    public Flux<Applications> getAllApplications() {
        return applicationReactiveRepo.findAll();

    }

    public Mono<ResponseEntity> createApplication(Applications applications1) {
        return applicationReactiveRepo
                .getAllByApplicationNameOrAndApplicationUrl(applications1.getApplicationName(), applications1.getApplicationUrl())
                .map(applications -> true)
                .switchIfEmpty(Mono.just(false))
                .filter(aBoolean -> !aBoolean)
                .flatMap(aBoolean -> applicationReactiveRepo.save(applications1))
                .map(applications -> userProvider.addApplicationToUsers(applications))
                .map(applications -> new ResponseEntity(applications, HttpStatus.OK))
                .switchIfEmpty(Mono.just(new ResponseEntity("U gjet", HttpStatus.CONFLICT)));
    }

    public Mono<ResponseEntity> addRoleToApplication(RoleDto roleDto) {
        return applicationReactiveRepo.getByApplicationUrl(roleDto.getApplicationUrl())
                .map(applications -> {
                    if (applications.getRoles() == null) {
                        ArrayList<String> roles = new ArrayList<>();
                        roles.add(roleDto.getRoleName());
                        applications.setRoles(roles);
                        return applications;
                    } else {
                        applications.getRoles().add(roleDto.getRoleName());
                        return applications;
                    }
                })
                .flatMap(applications -> applicationReactiveRepo.save(applications))
                .map(applicationsSingle -> new ResponseEntity(applicationsSingle, HttpStatus.OK))
                .switchIfEmpty(Mono.just(new ResponseEntity("no pm", HttpStatus.FORBIDDEN)));
    }

    public Mono<Applications> getApplicationByUrl(String web1test_url) {
        return applicationReactiveRepo.getByApplicationUrl(web1test_url);
    }

    public Mono<Void> updateApplication(Applications application) {
        return applicationReactiveRepo.findById(application.getId())
                .map(applications -> {
                    applications.setApplicationName(application.getApplicationName());
                    applications.setApplicationUrl(application.getApplicationUrl());
                    applications.setLogoutUrl(application.getLogoutUrl());
                    return applications;
                })
                .flatMap(applications -> applicationReactiveRepo.save(applications))
                .flatMap(applications -> userProvider.updateApplication(applications));
    }
}
