package com.tecnositaf.auth.controller;

import com.tecnositaf.auth.data.dto.TokenDto;
import com.tecnositaf.auth.data.model.AuthResponse;
import com.tecnositaf.auth.domain.config.SocketMessageComponent;
import com.tecnositaf.auth.domain.provider.LogoutExecutor;
import com.tecnositaf.auth.domain.provider.UserProvider;
import com.tecnositaf.auth.domain.security.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;


@RestController
@CrossOrigin
public class AuthController {
    @Autowired
    UserProvider userProvider;

    @Autowired
    TokenProvider tokenProvider;
    @Autowired
    SocketMessageComponent socketMessageComponent;
    @Autowired
    LogoutExecutor logoutExecutor;
    @Value("${security.jwt.token.expire-length}")
    private long validityInMilliseconds;

    @CrossOrigin
    @GetMapping(value = "/api/v1/logout")
    public Mono<AuthResponse> logOut(ServerHttpRequest servletRequest) {
        socketMessageComponent.sendMessage(tokenProvider.resolveToken(servletRequest));

        return Mono.just(tokenProvider.getEmail(tokenProvider.resolveToken(servletRequest)))
                .map(email -> {
                    userProvider.invalidateToken(email);
                    AuthResponse authResponse = new AuthResponse();
                    authResponse.setValidated(false);
                    authResponse.setUser(null);
                    return authResponse;
                });
    }

    @CrossOrigin
    @GetMapping(value = "/api/v1/auth")
    public Mono<AuthResponse> auth(ServerHttpRequest servletRequest) {

        return somePerrmissionSheats(servletRequest);
    }

    private Mono<AuthResponse> somePerrmissionSheats(ServerHttpRequest servletRequest) {
        return Mono.just(tokenProvider.resolveToken(servletRequest))
                .map(token -> tokenProvider.getEmail(token))
                .flatMap(email -> userProvider.getUserByEmail(email))
                .map(user -> userProvider.getUserDtoByUser(user, servletRequest.getHeaders().getFirst("appName")))
                .map(user -> {
                    AuthResponse authResponse = new AuthResponse();
                    authResponse.setValidated(true);
                    authResponse.setUser(user);
                    return authResponse;
                });
    }


    @CrossOrigin
    @GetMapping(value = "/api/v1/refreshToken")
    public Mono<ResponseEntity> refreshToken(ServerHttpRequest servletRequest) {
        return userProvider.getUserByToken(servletRequest)
                .flatMap(user -> {
                    if (tokenProvider.resolveToken(servletRequest).equals(user.getToken()) && !tokenProvider.validateToken(user.getToken()) && (user.getLastRequest() + validityInMilliseconds) > System.currentTimeMillis()) {
                        return userProvider.updateUserToken(user);
                    } else

                        return Mono.empty();
                })
                .map(s -> new ResponseEntity(new TokenDto(s), HttpStatus.OK))
                .switchIfEmpty(Mono.just(new ResponseEntity(new TokenDto("null"), HttpStatus.OK)));

    }
}
