package com.tecnositaf.auth.domain.provider;

import com.tecnositaf.auth.data.model.Applications;
import com.tecnositaf.auth.domain.config.SocketMessageComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;

@Component
public class LogoutExecutor {
    @Autowired
    SocketMessageComponent socketMessageComponent;
    @Autowired
    ApplicationProvider applicationProvider;

    @PostConstruct
    public void ListenForLogout() {
        socketMessageComponent
                .getSubscriber()
                .map(this::sendLogout)
                .subscribe();
    }

    private String sendLogout(String token) {
        getApplicationsWithUrl()
                .concatMapDelayError(applications -> logout(token, applications))
                .subscribe();
        return token;

    }

    private Mono<Object> logout(String token, Applications applications) {
        return WebClient.create(applications.getLogoutUrl())
                .post()
                .header("authorization", token)
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(Object.class));
    }

    private Flux<Applications> getApplicationsWithUrl() {
        return applicationProvider.getAllApplications()
                .filter(applications -> applications.getLogoutUrl() != null && applications.getLogoutUrl().length() > 1);
    }

}
