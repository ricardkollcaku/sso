package com.tecnositaf.auth.domain;

import com.tecnositaf.auth.data.model.Page;
import com.tecnositaf.auth.data.model.Pageable;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;

public abstract class PageProvider<T, R extends ReactiveMongoRepository<T, String>> {

    protected abstract ReactiveMongoTemplate getTemplate();

    protected abstract Class<T> getObjectClass();

    protected abstract R getRepository();

    public abstract void setRepository(R repository);



    private Mono<Long> getCount() {
        return getRepository().count();
    }

    private Mono<Long> getCountFilterd(Mono<Query> query, PageProvider<T, R> pageProvider) {
        return query.flatMap(query1 -> pageProvider.getTemplate().count(query1, pageProvider.getObjectClass()));

    }

    public Mono<Page<T>> getAll(Pageable pageable, HashMap<String, Object> entity,Flux<T> stream) {
        if (entity == null)
            return getPage(this, pageable,stream);
        else return getPageWithQuery(this, pageable, entity,stream);

    }

    private Mono<Page<T>> getPageWithQuery(PageProvider<T, R> pageProvider, Pageable pageable, HashMap<String, Object> entity,Flux<T> stream) {
        return Mono.just(pageable)
                .filter(Pageable::isNeedTotal)
                .flatMap(pageable1 -> getElementFilterdWithCount(pageProvider, pageable, queryBuilder(entity),stream))
                .switchIfEmpty(getElementsFilterd(pageProvider, pageable, queryBuilder(entity),stream));

    }

    private Mono<Page<T>> getElementsFilterd(PageProvider<T, R> pageProvider, Pageable pageable, Mono<Query> queryBuilder, Flux<T> stream) {
        return pageProvider.getPage(queryBuilder.flatMapMany(query -> pageProvider.getTemplate().find(query, pageProvider.getObjectClass())), pageable,stream);

    }

    private Mono<Page<T>> getPage(Flux<T> elements, Pageable pageable,Flux<T> stream) {
        return elements.skip(pageable.getPageNumber() * pageable.getElementPerPage())
                .take(pageable.getElementPerPage())
                .collectList()
                .map(ts -> {
                    Page<T> page = new Page<>();
                    page.setContent(ts);
                    page.setPageSize(pageable.getElementPerPage());
                    page.setPageNumber(pageable.getPageNumber());
                    return page;
                });
    }

    private Mono<Page<T>> getElementFilterdWithCount(PageProvider<T, R> pageProvider, Pageable pageable, Mono<Query> queryBuilder, Flux<T> stream) {
        return Mono.zip(getElementsFilterd(pageProvider, pageable, queryBuilder, stream), pageProvider.getCountFilterd(queryBuilder, pageProvider), (tPage, aLong) -> {
                    tPage.setTotalElements(aLong);
                    return tPage;
                }
        );
    }

    private Mono<Page<T>> getPage(PageProvider<T, R> pageProvider, Pageable pageable,Flux<T> stream) {
        return Mono.just(pageable)
                .filter(Pageable::isNeedTotal)
                .flatMap(pageable1 -> getElementWithCount(pageProvider, pageable,stream))
                .switchIfEmpty(getElements(pageProvider, pageable,stream));


    }

    private Mono<Page<T>> getElementWithCount(PageProvider<T, R> pageProvider, Pageable pageable,Flux<T> stream) {
        return Mono.zip(getElements(pageProvider, pageable,stream), pageProvider.getCount(), (tPage, aLong) -> {
                    tPage.setTotalElements(aLong);
                    return tPage;
                }
        );
    }

    private Mono<Page<T>> getElements(PageProvider<T, R> pageProvider, Pageable pageable,Flux<T> stream) {
        return pageProvider.getPage(stream, pageable,stream);

    }

    private Mono<Query> queryBuilder(HashMap<String, Object> entity) {
        Query query = new Query();

        return Flux.just(entity)
                .map(stringStringHashMap -> stringStringHashMap.entrySet())
                .flatMapIterable(entries -> entries)
                .map(stringStringEntry -> {
                    if (query.getQueryObject().get(stringStringEntry.getKey()) == null && stringStringEntry.getValue() != null && !String.valueOf(stringStringEntry.getValue()).equals(""))
                        query.addCriteria(buildCriteria(stringStringEntry.getKey(), stringStringEntry.getValue()));
                    return stringStringEntry;
                })
                .collectList()
                .map(entries -> query);
    }

    private CriteriaDefinition buildCriteria(String key, Object value) {
        if (value instanceof String)
            return Criteria.where(key).regex((String) value);
        else if (value instanceof Number)
            return Criteria.where(key).is((Number) value);
        else if (value instanceof Boolean)
            return Criteria.where(key).is((Boolean) value);
        return Criteria.where(key).is(value);
    }
}
