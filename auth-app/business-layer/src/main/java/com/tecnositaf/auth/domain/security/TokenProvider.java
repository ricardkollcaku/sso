package com.tecnositaf.auth.domain.security;

import com.tecnositaf.auth.data.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.Base64;
import java.util.Date;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Component
public class TokenProvider {


    @Value("${security.jwt.token.secret-key}")
    private String secretKey;
    @Value("${security.jwt.token.expire-length}")
    private long validityInMilliseconds;

    public Mono<String> resolveToken(ServerWebExchange serverWebExchange) {
        return Mono.justOrEmpty(serverWebExchange.getRequest()
                .getHeaders()
                .getFirst(HttpHeaders.AUTHORIZATION))
                .filter(s -> s.length() > 0 && s.startsWith("Bearer "))
                .map(s -> s.substring(7));
    }

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    public String createToken(User user) {
        System.out.println("TokenProvider create token " + user.getEmail());

        Claims claims = Jwts.claims().setSubject(user.getEmail());
        claims.put("name", user.getName());
        claims.put("lastName", user.getLastName());
        claims.put("username", user.getUserName());
        claims.put("email", user.getEmail());
        claims.put("auth", Stream.of(
                new SimpleGrantedAuthority(
                        user.getSystemRole()
                ))
                .collect(toList()));

        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();

    }


    public String getEmail(String token) {
        System.out.println("TokenProvider createToken get email");
        try {
            return token != null ? Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject() : null;
        } catch (ExpiredJwtException e) {
            return (String) e.getClaims().get("sub");
        }
    }

    public String resolveToken(ServerHttpRequest servletRequest) {
        String bearerToken = servletRequest.getHeaders().getFirst("authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }

        return null;
    }

    public boolean validateToken(String token) {
        System.out.println(" Token provider validating token");
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
