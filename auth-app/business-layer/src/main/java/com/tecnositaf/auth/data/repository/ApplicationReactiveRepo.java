package com.tecnositaf.auth.data.repository;

import com.tecnositaf.auth.data.model.Applications;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface ApplicationReactiveRepo extends ReactiveMongoRepository<Applications, String> {
    Mono<Applications> getAllByApplicationName(String applicationName);

    Mono<Applications> getAllByApplicationNameOrAndApplicationUrl(String applicationName, String applicationUrl);

    Mono<Applications> getByApplicationUrl(String url);
}
