package com.tecnositaf.auth.domain.provider;

import com.tecnositaf.auth.data.SystemRole;
import com.tecnositaf.auth.data.dto.*;
import com.tecnositaf.auth.data.model.Applications;
import com.tecnositaf.auth.data.model.Page;
import com.tecnositaf.auth.data.model.Pageable;
import com.tecnositaf.auth.data.model.User;
import com.tecnositaf.auth.data.repository.UserReactiveRepo;
import com.tecnositaf.auth.domain.PageProvider;
import com.tecnositaf.auth.domain.Util;
import com.tecnositaf.auth.domain.config.SocketMessageComponent;
import com.tecnositaf.auth.domain.security.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;

import static com.tecnositaf.auth.data.Strings.REGISTERERROR;

@Component
public class UserProvider extends PageProvider<User, UserReactiveRepo> {
    UserReactiveRepo userReactiveRepo;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    TokenProvider tokenProvider;
    @Autowired
    MongoOperations mongoOperation;
    @Autowired
    ApplicationProvider applicationProvider;
    @Autowired
    InMemoryProvider inMemoryProvider;
    @Autowired
    SocketMessageComponent socketMessageComponent;
    @Autowired
    Util util;
    @Autowired
    ReactiveMongoTemplate reactiveMongoTemplate;

    public Mono<User> getUserByEmail(String email) {
        if (email == null)
            return Mono.empty();
        if (inMemoryProvider.getUser(email) != null)
            return Mono.just(inMemoryProvider.getUser(email));
        return userReactiveRepo.findByEmail(email)
                .map(user -> {
                    inMemoryProvider.save(user);
                    return user;
                });
    }

    public Mono<User> getUserByEmailOrUsernameAndActive(String email, Boolean active) {
        if (inMemoryProvider.getUser(email) != null)
            return Mono.just(inMemoryProvider.getUser(email));
        return userReactiveRepo.findByEmailAndActiveOrUserNameAndActive(email, true, email, true)
                .map(user -> {
                    inMemoryProvider.save(user);
                    return user;
                });
    }

    public Flux<SimpleUserDto> getAllUsers(ServerHttpRequest httpServletRequest) {
        return getUserByToken(httpServletRequest)
                .flatMapMany(user -> {
                    if (user.getSystemRole().equals(SystemRole.ADMIN))
                        return userReactiveRepo.findAll();
                    return userReactiveRepo.findAllBySystemRole(SystemRole.USER);
                })
                .map(user -> {
                    SimpleUserDto simpleUserDto = new SimpleUserDto();
                    simpleUserDto.setId(user.getId());
                    simpleUserDto.setEmail(user.getEmail());
                    simpleUserDto.setName(user.getName());
                    return simpleUserDto;
                });
    }

    public Mono<Page<SimpleUserDto>> getAllUserPagable(Pageable pageable, ServerHttpRequest serverHttpRequest) {

        return getAllUsers(serverHttpRequest)
                .collectList()
                .map(simpleUserDtos -> {
                    Page<SimpleUserDto> simpleUserDtoPage = new Page<>();
                    simpleUserDtoPage.setPageSize(pageable.getElementPerPage());
                    simpleUserDtoPage.setPageNumber(pageable.getPageNumber());
                    simpleUserDtoPage.setContent(simpleUserDtos);
                    simpleUserDtoPage.setTotalElements(simpleUserDtos.size());
                    return simpleUserDtoPage;
                })

                .flatMap(simpleUserDtoPage -> Flux.fromIterable(simpleUserDtoPage.getContent())
                        .skip(pageable.getPageNumber() * pageable.getElementPerPage())
                        .take(pageable.getElementPerPage())
                        .collectList()
                        .map(simpleUserDtos -> {
                                    simpleUserDtoPage.setContent(simpleUserDtos);
                                    return simpleUserDtoPage;
                                }
                        ));

    }

    public Flux<UserDto> getAllUsersByApplication(ServerHttpRequest httpServletRequest) {
        return userReactiveRepo.findAllByActiveTrue()
                .map(user -> getUserDtoByUser(user, httpServletRequest.getHeaders().getFirst("appName")));
    }

    public Mono<Page<UserDto>> getAllUsersByApplicationPagable(Pageable pageable, ServerHttpRequest serverHttpRequest, String filter) {
        HashMap<String, Object> objectHashMap = util.parseStringToHashmap(filter);
        return getAll(pageable, objectHashMap,
                getUserByToken(serverHttpRequest)
                        .flatMapMany(user -> {
                            if (user.getSystemRole().equals(SystemRole.ADMIN))
                                return userReactiveRepo.findAll();
                            return userReactiveRepo.findAllBySystemRoleAndActiveTrue(SystemRole.USER);
                        }))
                .flatMap(userPage -> Flux.fromIterable(userPage.getContent())
                        .map(user -> getUserDtoByUser(user, serverHttpRequest.getHeaders().getFirst("appName")))
                        .collectList()
                        .map(userDtos -> {
                            Page<UserDto> page = new Page<>();
                            page.setContent(userDtos);
                            page.setPageNumber(userPage.getPageNumber());
                            page.setPageSize(userPage.getPageSize());
                            page.setTotalElements(userPage.getTotalElements());
                            return page;
                        })
                );


    }


    public Mono<String> login(String email, String password) {
        return getUserByEmailOrUsernameAndActive(email, true)
                .flatMap(user -> {
                    if (passwordEncoder.matches(password, user.getPassword()))
                        return Mono.just(user);
                    return Mono.empty();
                })
                .flatMap(user -> getToken(user))
                ;

    }

    public Mono<String> getToken(User user) {
        if (tokenProvider.validateToken(user.getToken())) {
            return Mono.just(user.getToken());
        } else {
            return updateUserToken(user);
        }
    }

    public Mono<String> updateUserToken(User user) {
        return Mono.just(user).map(user1 -> {
            Query query = new Query();
            String token = tokenProvider.createToken(user1);
            query.addCriteria(Criteria.where("email").is(user1.getEmail()));
            query.fields().include("email");
            Update update = new Update();
            update.set("token", token);
            user.setLastRequest(System.currentTimeMillis());
            user.setToken(token);
            inMemoryProvider.save(user);
            mongoOperation.updateFirst(query, update, User.class);
            return token;
        });
    }

    public Mono<ResponseEntity> createUser(User user) {
        user.setApplications(new ArrayList<>());
        System.out.println("duke save user");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userReactiveRepo.
                findByEmail(user.getEmail())
                .map(user1 -> true)
                .switchIfEmpty(Mono.just(false))
                .filter(aBoolean -> !aBoolean)
                .flatMap(aBoolean -> applicationProvider.getAllApplications()
                        .map(applications -> {
                            applications.setRoles(new ArrayList<>());
                            return applications;
                        })
                        .collectList()
                )
                .map(applications -> {
                    user.setApplications((ArrayList<Applications>) applications);
                    return user;

                })
                .flatMap(user1 -> userReactiveRepo.save(user1))
                .map(user1 -> new ResponseEntity(new TokenDto(tokenProvider.createToken(user1)), HttpStatus.OK))
                .map(responseObject -> {
                    Query query = new Query();
                    query.addCriteria(Criteria.where("email").is(user.getEmail()));
                    query.fields().include("email");
                    Update update = new Update();
                    update.set("token", responseObject.getBody());
                    mongoOperation.updateFirst(query, update, User.class);
                    return responseObject;
                })
                .switchIfEmpty(Mono.just(new ResponseEntity(REGISTERERROR, HttpStatus.FORBIDDEN)));
    }

    public void invalidateToken(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));
        query.fields().include("email");
        Update update = new Update();
        update.set("token", "");
        mongoOperation.updateFirst(query, update, User.class);
        inMemoryProvider.remove(email);
    }

    public Mono<User> getUserById(String id) {
        return userReactiveRepo.findById(id);
    }

    public Mono<User> saveUser(User user) {
        return userReactiveRepo.save(user);
    }

    public Mono<ResponseEntity> updateUser(ResponseUserDto user) {
        return userReactiveRepo.findByEmail(user.getEmail())
                .map(user1 -> {
                    user1.setName(user.getName());
                    user1.setActive(user.isActive());
                    return user1;
                })
                .flatMap(user1 -> getUserWithFixetRoles(user1, user))

                .flatMap(user1 -> saveUser(user1))
                .map(user1 -> {
                    socketMessageComponent.sendMessage(user1.getToken());
                    return user1;
                })
                .map(user1 -> new ResponseEntity(user1, HttpStatus.OK));
    }

    private Mono<User> getUserWithFixetRoles(User user1, ResponseUserDto user) {
        return Mono.just(user1)
                .filter(user2 -> user2.getApplications() != null)
                .flatMapMany(user2 -> Flux.fromIterable(user2.getApplications()))
                .map(applications -> {
                    if (applications.getApplicationUrl().equals(user.getApplications()))
                        applications.setRoles(user.getRoles());
                    return applications;
                })
                .collectList()
                .map(applications -> {
                    user1.setApplications((ArrayList<Applications>) applications);
                    return user1;
                });
    }

    public Applications addApplicationToUsers(Applications applications) {
        Update update = new Update();
        applications.setRoles(new ArrayList<>());
        update.addToSet("applications", applications);
        Criteria criteria = Criteria.where("_id").exists(true);
        mongoOperation.updateMulti(Query.query(criteria), update, User.class);
        return applications;
    }

    public UserDto getUserDtoByUser(User user, String appName) {
        UserDto userDto = new UserDto();
        userDto.setEmail(user.getEmail());
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setUserName(user.getUserName());
        userDto.setLastName(user.getLastName());
        if (appName != null && !appName.equals(""))
            for (Applications applications : user.getApplications())
                if (applications.getApplicationName().equals(appName)) {
                    userDto.setRoles(applications.getRoles());
                    break;
                }
        return userDto;
    }

    public LogedInUserDto getFrontUserByUSer(User user) {
        LogedInUserDto logedInUserDto = new LogedInUserDto();
        logedInUserDto.setEmail(user.getEmail());
        logedInUserDto.setId(user.getId());
        logedInUserDto.setLastName(user.getLastName());
        logedInUserDto.setName(user.getName());
        logedInUserDto.setUserName(user.getUserName());
        logedInUserDto.setSystemRole(user.getSystemRole());
        logedInUserDto.setMyApplication(user.getMyApplication());
        return logedInUserDto;
    }

    public Mono<User> getUserByRole(String admin) {
        return userReactiveRepo.findBySystemRole(admin);
    }

    public Mono<User> getUserByToken(ServerHttpRequest servletRequest) {
        return getUserByEmail(tokenProvider.getEmail(tokenProvider.resolveToken(servletRequest)));

    }

    public Mono<User> getUserByUsername(String username) {
        return userReactiveRepo.findByUserName(username);
    }

    public Mono<User> changePass(ServerHttpRequest serverHttpRequest, ChangePassDto changePassDto) {
        return getUserByToken(serverHttpRequest)
                .filter(user -> passwordEncoder.matches(changePassDto.getOldPass(), user.getPassword()))
                .map(user -> {
                    user.setPassword(passwordEncoder.encode(changePassDto.getNewPass()));
                    return user;
                })
                .flatMap(user -> userReactiveRepo.save(user))
                .map(user -> {
                    inMemoryProvider.save(user);
                    return user;
                });
    }

    public Mono<Void> updateApplication(Applications applications) {
        return userReactiveRepo.findAll()
                .flatMap(user -> {
                    if (user.getMyApplication() != null && user.getMyApplication().getId().equals(applications)) {
                        user.getMyApplication().setApplicationName(applications.getApplicationName());
                        user.getMyApplication().setApplicationUrl(applications.getApplicationUrl());
                        return userReactiveRepo.save(user);
                    }
                    return Mono.just(user);
                })
                .flatMap(user -> Flux.fromIterable(user.getApplications())
                        .map(applications1 -> {
                            if (applications.getId().equals(applications1.getId())) {
                                applications1.setApplicationUrl(applications.getApplicationUrl());
                                applications1.setApplicationName(applications.getApplicationName());
                            }
                            return applications1;
                        })
                        .collectList()
                        .map(applications1 -> {
                            user.setApplications((ArrayList<Applications>) applications1);
                            return user;
                        })
                        .flatMap(user1 -> userReactiveRepo.save(user1))
                )
                .collectList()
                .then();
    }

    @Override
    protected ReactiveMongoTemplate getTemplate() {
        return reactiveMongoTemplate;
    }

    @Override
    protected Class<User> getObjectClass() {
        return User.class;
    }

    @Override
    protected UserReactiveRepo getRepository() {
        return userReactiveRepo;
    }

    @Autowired
    public void setRepository(UserReactiveRepo repository) {
        this.userReactiveRepo = repository;
    }

    public Mono<User> saveIfNotExist(User user) {
        return getUserById(user.getId())
                .switchIfEmpty(saveUser(user));
    }
}
