package com.tecnositaf.auth.data.repository;

import com.tecnositaf.auth.data.model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public interface UserReactiveRepo extends ReactiveMongoRepository<User, String> {
    Mono<User> findByEmail(String email);

    //Mono<User> findById(String id);

    Mono<User> findByEmailAndActive(String email, Boolean active);

    Mono<User> findByEmailAndActiveOrUserNameAndActive(String email, Boolean active, String username, Boolean active1);

    Mono<User> findBySystemRole(String role);

    Flux<User> findAllBySystemRole(String role);
    Flux<User> findAllBySystemRoleAndActiveTrue(String sysrole);

    Mono<User> findByUserName(String username);

    Flux<User> findAllByActiveTrue();


}
