package com.tecnositaf.auth.data;

import org.springframework.http.HttpStatus;

public class ResponseObject {
    private Integer status;
    private Object body;

    public ResponseObject(HttpStatus status, Object body) {
        this.status = status.value();
        this.body = body;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status.value();
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}
