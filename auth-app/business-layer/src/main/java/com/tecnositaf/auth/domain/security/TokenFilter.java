package com.tecnositaf.auth.domain.security;

import com.tecnositaf.auth.domain.config.SocketMessageComponent;
import com.tecnositaf.auth.domain.provider.InMemoryProvider;
import com.tecnositaf.auth.domain.provider.UserProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Component
public class TokenFilter implements Function<ServerWebExchange, Mono<Authentication>> {
    @Autowired
    TokenProvider tokenProvider;
    @Autowired
    UserProvider userProvider;
    @Autowired
    InMemoryProvider inMemoryProvider;
    @Autowired
    SocketMessageComponent socketMessageComponent;

    @Override
    public Mono<Authentication> apply(ServerWebExchange serverWebExchange) {
        return Mono.justOrEmpty(serverWebExchange)
                .flatMap(serverWebExchange1 -> tokenProvider.resolveToken(serverWebExchange1))
                .flatMap(s -> getAuthenticationRefreshingTime(s))
                .log();
    }

    public Mono<Authentication> getAuthenticationRefreshingTime(String token) {
        return userProvider.getUserByEmailOrUsernameAndActive(tokenProvider.getEmail(token), true)
                .flatMap(user -> {
                    user.setLastRequest(System.currentTimeMillis());
                    inMemoryProvider.save(user);
                    if (user == null || !user.getToken().equals(token)) {
                        socketMessageComponent.sendMessage(token);
                        return Mono.empty();
                    }
                    return Mono.just(new UsernamePasswordAuthenticationToken(user, "", Stream.of(new SimpleGrantedAuthority(user.getSystemRole())).collect(toList())));
                });

    }


/*
    @Autowired
    TokenProvider tokenProvider;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String token = tokenProvider.resolveToken((ServerHttpRequest) servletRequest);
        System.out.println("tokeni :"+token);
        if (token != null && tokenProvider.validateToken(token)) {
            SecurityContextHolder.getContext().setAuthentication(tokenProvider.getAuthenticationRefreshingTime(token));
        } else {
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
*/


}
